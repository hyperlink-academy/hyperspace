import f, { Expr } from "faunadb";
let { query: q } = f;

export function GetIfExists(Set: Expr, selection?: string[]) {
  return q.Let(
    {
      Set,
    },
    q.If(
      q.Exists(q.Var("Set")),
      selection
        ? q.Select(selection, q.Get(q.Var("Set")))
        : q.Get(q.Var("Set")),
      null
    )
  );
}
