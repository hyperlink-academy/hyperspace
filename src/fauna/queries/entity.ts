import { values, query as q, Expr } from "faunadb";
import { Block, blockQuery } from "src/fauna/queries/block";
import { generateKeyBetween } from "src/fractional-indexing";
import { CreateEntityFunction } from "../setup/Entities";
import { CreateFactFunction, Fact, Facts, ScanIndex } from "../setup/Facts";
import { GetIfExists } from "../utils";

export type Entity = {
  ref: values.Ref;
  title: { factRef: values.Ref; value: string } | null;
  icon: { factRef?: values.Ref; value: string } | null;
  blocks: Block[];
  backReferences: {
    parent: {
      title: string | false;
      ref: values.Ref;
      icon: { data: Fact; ref: values.Ref } | null;
    };
    block: Block;
  }[];
};

export const entityQuery = (entity: Expr) => {
  return q.Let(
    {
      entity,
      title: q.Let(
        {
          fact: GetIfExists(ScanIndex("eav", q.Var("entity"), "content/title")),
        },
        q.If(q.IsNull(q.Var("fact")), null, {
          factRef: q.Select("ref", q.Var("fact")),
          value: q.Select(["data", "value"], q.Var("fact")),
        })
      ),
    },
    q.If(
      q.Exists(q.Var("entity")),
      {
        ref: q.Var("entity"),
        title: q.Var("title"),
        icon: q.Let(
          {
            fact: GetIfExists(
              ScanIndex("eav", q.Var("entity"), "content/icon")
            ),
          },
          q.If(q.IsNull(q.Var("fact")), null, {
            factRef: q.Select("ref", q.Var("fact")),
            value: q.Select(["data", "value"], q.Var("fact")),
          })
        ),
        blocks: q.Map(
          q.Select(
            "data",
            q.Paginate(
              q.Range(
                q.Match(q.Index(Facts.indexes.vae.name), [q.Var("entity")]),
                "block/parent",
                "block/parent"
              ),
              { size: 1000 }
            )
          ),
          q.Lambda((vae) => blockQuery(q.Select(1, vae) as values.Ref))
        ),
        backReferences: q.If(
          q.IsNull(q.Var("title")),
          [],
          q.Map(
            //These are going to be blocks, so I have to look up their parents, and then *their* titles
            q.Select(
              "data",
              q.Paginate(
                ScanIndex(
                  "ave",
                  "content/link",
                  q.Select("value", q.Var("title"))
                )
              )
            ),
            q.Lambda((backRef) =>
              q.Let(
                {
                  block: blockQuery(q.Select(1, backRef) as values.Ref),
                  parent: GetIfExists(
                    ScanIndex("eav", q.Select([1], backRef), "block/parent"),
                    ["data", "value"]
                  ),
                  icon: q.If(
                    q.IsNull(q.Var("parent")),
                    null,
                    GetIfExists(
                      ScanIndex("eav", q.Var("parent"), "content/icon")
                    )
                  ),
                  title: q.If(
                    q.IsNull(q.Var("parent")),
                    null,
                    q.Select(
                      ["data", "value"],
                      q.Get(ScanIndex("eav", q.Var("parent"), "content/title"))
                    )
                  ),
                },
                {
                  parent: {
                    title: q.Var("title"),
                    icon: q.Var("icon"),
                    ref: q.Var("parent"),
                  },
                  block: q.Var("block"),
                }
              )
            )
          )
        ),
      },
      null
    )
  );
};
export const CreateNewEntity = (attributes: { [a: string]: string }) => {
  return q.Let(
    {
      entity: q.Select("ref", CreateEntityFunction.call({ facts: {} })),
      firstBlockEntity: q.Select(
        "ref",
        CreateEntityFunction.call({ facts: {} })
      ),
    },
    q.Do(
      ...Object.keys(attributes).map((a) => {
        return CreateFactFunction.call({
          entity: q.Var("entity") as values.Ref,
          attribute: a,
          value: attributes[a],
        });
      }),
      CreateFactFunction.call({
        entity: q.Var("firstBlockEntity") as values.Ref,
        attribute: "content/text",
        value: "",
      }),
      CreateFactFunction.call({
        entity: q.Var("firstBlockEntity") as values.Ref,
        attribute: "block/position",
        value: generateKeyBetween(null, null),
      }),
      CreateFactFunction.call({
        entity: q.Var("firstBlockEntity") as values.Ref,
        attribute: "block/parent",
        value: q.Var("entity") as values.Ref,
      }),

      entityQuery(q.Var("entity"))
    )
  );
};
