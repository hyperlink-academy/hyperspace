import { values, query as q } from "faunadb";
import { Facts, ScanIndex } from "../setup/Facts";
import { GetIfExists } from "../utils";

export type Block = {
  contentText?: { factRef?: values.Ref; value: string };
  contentImage?: { factRef?: values.Ref; value: string };
  position: { factRef?: values.Ref; value: string };
  blockType?: {
    ref: values.Ref;
    data: { attribute: "text/type"; value: string };
  };
  blockEntity?: values.Ref;
  links: { title: string; entity: values.Ref; icon: string }[];
  tempID?: string;
};

export const blockQuery = (blockEntity: values.Ref) =>
  q.Let(
    {
      attributes: q.Match(q.Index(Facts.indexes.eav.name), [blockEntity]),
      links: q.Map(
        q.Select(
          "data",
          q.Paginate(
            q.Range(q.Var("attributes"), "content/link", "content/link")
          )
        ),
        q.Lambda((link) =>
          q.Let(
            {
              title: q.Select(1, link),
              entity: ScanIndex("ave", "content/title", q.Var("title")),
            },
            q.If(
              q.Exists(q.Var("entity")),
              q.Let(
                {
                  entityRef: q.Select(
                    ["data", "entity"],
                    q.Get(q.Var("entity"))
                  ),
                },
                {
                  entity: q.Var("entityRef"),
                  title: q.Var("title"),
                  icon: q.Let(
                    {
                      fact: GetIfExists(
                        ScanIndex("eav", q.Var("entityRef"), "content/icon")
                      ),
                    },
                    q.Select(["data", "value"], q.Var("fact"), "")
                  ),
                }
              ),
              null
            )
          )
        )
      ),
      contentImage: q.Select(
        ["data", 0],
        q.Paginate(
          q.Range(q.Var("attributes"), "content/image", "content/image")
        ),
        false
      ),
      contentText: q.Select(
        ["data", 0],
        q.Paginate(
          q.Range(q.Var("attributes"), "content/text", "content/text")
        ),
        false
      ),
      position: q.Select(
        ["data", 0],
        q.Paginate(
          q.Range(q.Var("attributes"), "block/position", "block/position")
        ),
        ""
      ),
    },
    {
      blockEntity,
      links: q.Var("links"),
      blockType: GetIfExists(
        q.Range(q.Var("attributes"), "text/type", "text/type")
      ),
      contentText: q.If(q.Equals(q.Var("contentText"), false), null, {
        value: q.Select([1], q.Var("contentText")),
        factRef: q.Select(2, q.Var("contentText")),
      }),
      contentImage: q.If(q.Equals(q.Var("contentImage"), false), null, {
        value: q.Select([1], q.Var("contentImage")),
        factRef: q.Select(2, q.Var("contentImage")),
      }),
      position: {
        value: q.Select([1], q.Var("position")),
        factRef: q.Select(2, q.Var("position")),
      },
    }
  );

export const updateContentQuery = (
  factRef: values.Ref,
  value: string,
  blockEntity: values.Ref
) => {
  let links = extractLinks(value).map((s) => s[1]);
  return q.Do(
    q.Call("UpdateFact", factRef, value),
    q.Foreach(
      q.Select(
        "data",
        q.Paginate(
          q.Range(
            q.Match(q.Index("eav"), blockEntity),
            "content/link",
            "content/link"
          )
        ),
        { size: 1000 }
      ),
      q.Lambda((link) => q.Call("DeleteFact", q.Select(2, link)))
    ),
    q.Foreach(
      links || [],
      q.Lambda((newLink) =>
        q.Call("CreateFact", {
          entity: blockEntity,
          attribute: "content/link",
          value: newLink,
        })
      )
    )
  );
};

export function extractLinks(value: string) {
  return [...value.matchAll(/\[\[([^\[\n\]]*)\]\]/g)];
}
