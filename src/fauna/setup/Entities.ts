import f, { values } from "faunadb";
import * as Y from "yjs";
import { CreateFactFunction, ScanIndex } from "./Facts";
let { query: q } = f;

type Entity = {};

export const Entities = {
  name: "Entities",
  indexes: {},
  privilege: {
    actions: {
      read: true, // TODO add permissions
    },
  },
};

export const DeleteEntityFunction = {
  name: "DeleteEntity",
  call: (entity: values.Ref) => q.Call("DeleteEntity", entity),
  role: "server",
  privilege: {
    call: true, //TODO Let only the owner delete an entity
  },
  body: q.Query(
    q.Lambda((entity) =>
      q.Do(
        q.Foreach(
          q.Paginate(ScanIndex("eav", entity)),
          q.Lambda((fact) => q.Delete(q.Select(2, fact)))
        ),
        q.Delete(entity)
      )
    )
  ),
} as const;

export const CreateEntityFunction = {
  name: "CreateEntity",
  call: (entity: Entity) => {
    let CRDT = new Y.Doc();
    let bytes = Y.encodeStateAsUpdate(CRDT);
    return q.Let(
      { entity: q.Call("CreateEntity", entity) },
      q.Do(
        CreateFactFunction.call({
          entity: q.Select("ref", q.Var("entity")) as values.Ref,
          attribute: "doc/crdt",
          value: Buffer.from(bytes).toString("base64"),
        }),
        q.Var("entity")
      )
    );
  },
  role: "server",
  body: q.Query(
    q.Lambda((entity) =>
      q.Create(q.Collection(Entities.name), { data: entity })
    )
  ),
  privilege: {
    call: true,
  },
} as const;

export const UpdateEntityBlockFunction = {
  name: "UpdateEntityBlock",
  call: (entity: values.Ref, fact: values.Ref, position: string) =>
    q.Call("UpdateEntityBlock", entity, fact, position),
  role: "server",
  body: q.Query(
    q.Lambda((entity, fact, position) =>
      q.Update(entity, {
        data: { facts: q.ToObject([[q.Select(["id"], fact), position]]) },
      })
    )
  ),
  privilege: {
    call: true,
  },
} as const;
