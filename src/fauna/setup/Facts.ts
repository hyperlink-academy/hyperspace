import f, { Expr, values } from "faunadb";
import { GetIfExists } from "../utils";
let { query: q } = f;

let nums = (function () {
  let numbers = [];
  for (let i = 0; i < 140; i++) numbers.push(i);
  return numbers;
})();

export const Facts = {
  name: "Facts",
  indexes: {
    search: {
      name: "search",
      bindings: {
        prefixes: q.Query(
          q.Lambda((fact) =>
            q.If(
              q.Equals(q.Select(["data", "attribute"], fact), "content/title"),
              q.Map(
                q.Take(q.Length(q.Select(["data", "value"], fact)), nums),
                q.Lambda((len) =>
                  q.Casefold(
                    q.SubString(q.Select(["data", "value"], fact), len)
                  )
                )
              ),
              null
            )
          )
        ),
      },
      serialized: false,
      values: [
        { binding: "prefixes" },
        { field: ["data", "value"] },
        { field: ["data", "entity"] },
      ],
    },
    eav: {
      name: "eav",
      terms: [{ field: ["data", "entity"] }],
      values: [
        { field: ["data", "attribute"] },
        { field: ["data", "value"] },
        { field: ["ref"] },
      ],
    },
    aev: {
      name: "aev",
      terms: [{ field: ["data", "attribute"] }],
      values: [
        { field: ["data", "entity"] },
        { field: ["data", "value"] },
        { field: ["ref"] },
      ],
    },
    vae: {
      name: "vae",
      bindings: {
        value: q.Query(
          q.Lambda(
            "fact",
            q.Let(
              { ref: q.Select(["data", "value"], q.Var("fact")) },
              q.If(q.IsRef(q.Var("ref")), q.Var("ref"), null)
            )
          )
        ),
      },
      terms: [{ binding: "value" }],
      values: [
        { field: ["data", "attribute"] },
        { field: ["data", "entity"] },
        { field: ["ref"] },
      ],
    },
    unique: {
      name: "unique",
      unique: true,
      bindings: {
        attribute: q.Query(
          q.Lambda(
            "fact",
            q.If(
              q.Select(["data", "metadata", "unique"], q.Var("fact"), false),
              q.Select(["data", "attribute"], q.Var("fact")),
              null
            )
          )
        ),
        value: q.Query(
          q.Lambda(
            "fact",
            q.If(
              q.Select(["data", "metadata", "unique"], q.Var("fact"), false),
              q.Select(["data", "value"], q.Var("fact")),
              null
            )
          )
        ),
      },
      terms: [{ binding: "attribute" }],
      values: [{ binding: "value" }],
    },
    ave: {
      name: "ave",
      bindings: {
        attribute: q.Query(
          q.Lambda(
            "fact",
            q.If(
              q.Or(
                q.Select(["data", "metadata", "unique"], q.Var("fact"), false),
                q.Select(["data", "metadata", "index"], q.Var("fact"), false)
              ),
              q.Select(["data", "attribute"], q.Var("fact")),
              null
            )
          )
        ),
      },
      terms: [{ binding: "attribute" }],
      values: [
        { field: ["data", "value"] },
        { field: ["data", "entity"] },
        { field: ["ref"] },
      ],
    },
  },
  privilege: {
    actions: {
      read: true, //TODO Implement permissions
    },
  },
} as const;

export function ScanIndex(
  index: typeof Facts["indexes"][keyof typeof Facts["indexes"]]["name"],
  shard: string | values.Ref | Expr,
  start?: string | values.Ref | Expr,
  end?: string | values.Ref | Expr
) {
  if (start)
    return q.Range(q.Match(q.Index(index), shard), start, end || start);
  return q.Match(q.Index(index), shard);
}

export function UpdateOrCreateFact(
  entity: values.Ref,
  attribute: string,
  value: string
) {
  return q.Let(
    {
      factSet: GetIfExists(ScanIndex("eav", entity, attribute)),
    },
    q.If(
      q.IsNull(q.Var("factSet")),
      CreateFactFunction.call({ entity, attribute, value }),
      UpdateFactFunction.call(q.Select("ref", q.Var("factSet")), value)
    )
  );
}

export type Fact = {
  entity: values.Ref;
  attribute: string;
  value: string | boolean | values.Ref;
};

// In order to create a fact I first need to lookup the attribute and
// see if it has any schema things associated with it

export const CreateFactFunction = {
  name: "CreateFact",
  role: "server",
  call: (fact: Fact) => q.Call("CreateFact", fact),
  body: q.Query(
    q.Lambda((fact) =>
      q.If(
        q.Exists(q.Select("entity", fact)),
        q.Let(
          {
            entity: q.Get(q.Select("entity", fact)),
            attribute: ScanIndex(
              "ave",
              "attribute/identifier",
              q.Select("attribute", fact)
            ),
            attributeMetadata: q.If(
              q.Exists(q.Var("attribute")),
              q.Let(
                {
                  attributeEntity: q.Select(
                    ["data", "entity"],
                    q.Get(q.Var("attribute"))
                  ),
                  attributeFacts: q.ToObject(
                    q.Map(
                      q.Select(
                        "data",
                        q.Paginate(
                          ScanIndex(
                            "eav",
                            q.Var("attributeEntity"),
                            "attribute",
                            "attribute" + String.fromCharCode(65535)
                          )
                        ),
                        []
                      ),
                      q.Lambda((f) =>
                        q.Append(
                          [],
                          [q.SubString(q.Select(0, f), 10), q.Select(1, f)]
                        )
                      )
                    )
                  ),
                },
                q.Var("attributeFacts")
              ),
              {}
            ),
          },
          q.Create(q.Collection(Facts.name), {
            data: q.Merge(fact, {
              metadata: q.Var("attributeMetadata"),
            }),
          })
        ),
        null
      )
    )
  ),
  privilege: {
    call: true,
  },
} as const;

// You don't need to look up the schema when updating a fact as the metadata has
// already been associated with it

export const UpdateFactFunction = {
  name: "UpdateFact",
  role: "server",
  call: (factRef: Expr, value: string | Expr | boolean) =>
    q.Call("UpdateFact", factRef, value),
  body: q.Query(
    q.Lambda((factRef, value) => q.Update(factRef, { data: { value } }))
  ),
  privilege: {
    call: true,
  },
} as const;

export const DeleteFactFunction = {
  name: "DeleteFact",
  role: "server",
  call: (factRef: Expr) => q.Call("DeleteFact", factRef),
  body: q.Query(q.Lambda((factRef) => q.Delete(factRef))),
  privilege: {
    call: true,
  },
} as const;

export const CreateOrUpdateFact = (
  entity: values.Ref,
  attribute: string,
  value: string | boolean | values.Ref
) =>
  q.Let(
    {
      fact: GetIfExists(ScanIndex("eav", entity, attribute)),
    },
    q.If(
      q.IsNull(q.Var("fact")),
      CreateFactFunction.call({ entity, attribute, value }),
      UpdateFactFunction.call(q.Select("ref", q.Var("fact")), value)
    )
  );
