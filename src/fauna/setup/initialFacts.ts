import f, { values } from "faunadb";
import { CreateEntityFunction } from "./Entities";
import { CreateFactFunction, Facts } from "./Facts";
let { query: q, Client } = f;
export const initialFacts: { [attr: string]: string | boolean }[] = [
  {
    "attribute/identifier": "attribute/identifier",
    "attribute/unique": true,
  },
  {
    "attribute/identifier": "attribute/unique",
  },
  {
    "attribute/identifier": "attribute/index",
  },
  {
    "attribute/identifier": "block/parent",
  },
  {
    "attribute/identifier": "content/title",
    "attribute/unique": true,
  },
  {
    "attribute/identifier": "content/link",
    "attribute/index": true,
  },
  {
    "attribute/identifier": "content/text",
  },
];

async function main() {
  let client = new Client({ secret: process.env.FAUNA_ADMIN_KEY || "" });
  try {
    client
      .query(
        q.Do(
          q.Do(
            q.Foreach(
              q.Select("data", q.Paginate(q.Documents(q.Collection("Facts")))),
              q.Lambda((collection) => q.Delete(collection))
            ),
            q.Foreach(
              q.Select(
                "data",
                q.Paginate(q.Documents(q.Collection("Entities")))
              ),
              q.Lambda((collection) => q.Delete(collection))
            )
          ),
          ...initialFacts.map((f) => {
            let attributes = Object.keys(f);
            return q.Let(
              { entity: q.Select("ref", CreateEntityFunction.call({})) },
              q.Do(
                ...attributes.map((a) => {
                  if (
                    a === "attribute/identifier" &&
                    f[a] === "attribute/identifier"
                  ) {
                    console.log("yooo");
                    return q.Create(q.Collection(Facts.name), {
                      data: {
                        entity: q.Var("entity") as values.Ref,
                        attribute: a,
                        value: f[a],
                        metadata: { unique: true },
                      },
                    });
                  }
                  return CreateFactFunction.call({
                    entity: q.Var("entity") as values.Ref,
                    attribute: a,
                    value: f[a],
                  });
                })
              )
            );
          })
        )
      )
      .catch((e) =>
        console.log(e.requestResult.responseContent.errors[0].cause)
      );
  } catch (e) {
    console.log(e);
  }
}

main();
