import f from "faunadb";
import { Expr, ExprArg } from "faunadb";
import {
  CreateEntityFunction,
  DeleteEntityFunction,
  Entities,
  UpdateEntityBlockFunction,
} from "./Entities";
import {
  CreateFactFunction,
  DeleteFactFunction,
  Facts,
  UpdateFactFunction,
} from "./Facts";
let { Client, query: q } = f;
import { CreatePersonFunction, People, TemporaryLogin } from "./People";
import { SSOTokens } from "./SSOTokens";
export type Collection = {
  name: string;
  privilege: {
    actions?: {
      read?: boolean | Expr;
      write?: boolean | Expr;
    };
  };
  indexes: {
    [name: string]: {
      name: string;
      unique?: boolean;
      serialized: boolean;
      bindings?: { [k: string]: Expr };
      terms: Array<{ field: string[] } | { binding: string }>;
      values?: Array<{ field: string[] } | { binding: string }>;
    };
  };
};

export type FaunaFunction = {
  name: string;
  body: Expr;
  role: "server";
  privilege: {
    call: boolean | Expr;
  };
};

let FaunaFunctions: FaunaFunction[] = [
  CreatePersonFunction,
  CreateEntityFunction,
  DeleteEntityFunction,
  UpdateEntityBlockFunction,
  CreateFactFunction,
  DeleteFactFunction,
  UpdateFactFunction,
];

let Collections: Collection[] = [
  People,
  Entities,
  Facts,
  SSOTokens,
  TemporaryLogin,
];

export const initialFacts = [
  {
    "attribute/identifier": "attribute/identifier",
    "attribute/unique": true,
  },
  {
    "attribute/identifier": "attribute/unique",
  },
  {
    "attribute/identifier": "block/parent",
  },
  {
    "attribute/identifier": "content/title",
  },
  {
    "attribute/identifier": "content/text",
  },
];

export async function init() {
  if (!process.env.FAUNA_ADMIN_KEY) {
    console.log("no FAUNA_ADMIN_KEY");
    return;
  }
  let client = new Client({ secret: process.env.FAUNA_ADMIN_KEY || "" });

  // 1. Create Collections
  console.log("Creating collections and indexes");
  await Promise.all(
    Collections.map(async (collection) => {
      await client.query(
        q.If(
          q.Exists(q.Collection(collection.name)),
          true,
          q.CreateCollection({ name: collection.name })
        )
      );
      for (let name in collection.indexes) {
        let index = collection.indexes[name];
        try {
          await client.query(
            q.If(
              q.Exists(q.Index(name)),
              true,
              q.CreateIndex({
                name: index.name,
                source: !index.bindings
                  ? q.Collection(collection.name)
                  : {
                      collection: q.Collection(collection.name),
                      fields: index.bindings,
                    },
                terms: index.terms,
                serialized: index.serialized,
                values: index.values,
                unique: index.unique,
              })
            )
          );
        } catch (e) {
          console.log(e);
        }
      }
    })
  );

  // 2. Create the functions
  console.log("Creating functions");
  await Promise.all(
    FaunaFunctions.map((func) =>
      client.query(
        CreateOrUpdateFunction({
          name: func.name,
          body: func.body,
          role: func.role,
        })
      )
    )
  );

  // 3. Create the roles
  console.log("Creating or updating role");
  try {
    await client.query(
      CreateOrUpdateRole({
        name: "person",
        membership: {
          resource: q.Collection("People"),
        },
        privileges: [
          ...Collections.map((collection) => {
            return {
              resource: q.Collection(collection.name),
              actions: collection.privilege.actions,
            };
          }),
          ...Collections.flatMap((collection) =>
            Object.keys(collection.indexes).map((indexName) => {
              return { resource: q.Index(indexName), actions: { read: true } }; // People can read all indexes
            })
          ),
          ...FaunaFunctions.map((f) => {
            return { resource: q.Function(f.name), actions: f.privilege };
          }),
        ],
      })
    );

    await client.query(
      CreateOrUpdateRole({
        name: "temporary-login",
        membership: {
          resource: q.Collection("TemporaryLogin"),
        },
        privileges: [
          ...Collections.map((collection) => {
            return {
              resource: q.Collection(collection.name),
              actions: collection.privilege.actions,
            };
          }),
          ...Collections.flatMap((collection) =>
            Object.keys(collection.indexes).map((indexName) => {
              return { resource: q.Index(indexName), actions: { read: true } };
            })
          ),
          // TempLogins can't call any functions!
        ],
      })
    );
  } catch (e) {
    console.log(e.requestResult.responseContent.errors[0].position);
  }
}

try {
  init();
} catch (e) {
  console.log(e);
}

export function CreateOrUpdateFunction(obj: {
  name: string;
  body: ExprArg;
  role: ExprArg;
}) {
  return q.If(
    q.Exists(q.Function(obj.name)),
    q.Update(q.Function(obj.name), { body: obj.body, role: obj.role }),
    q.CreateFunction({ name: obj.name, body: obj.body, role: obj.role })
  );
}

export function CreateOrUpdateRole(obj: {
  name: string;
  membership: ExprArg;
  privileges: ExprArg;
}) {
  return q.If(
    q.Exists(q.Role(obj.name)),
    q.Update(q.Role(obj.name), {
      membership: obj.membership,
      privileges: obj.privileges,
    }),
    q.CreateRole(obj)
  );
}
