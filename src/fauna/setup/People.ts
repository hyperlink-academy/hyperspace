import f from "faunadb";
import { CreateEntityFunction } from "./Entities";
let { query: q } = f;

// SCHEMA {
//   id: string -- Reference to id in postgres
//   homepage: ref == Entities
// }
export const People = {
  name: "People",
  indexes: {
    people_by_id: {
      name: "people_by_id",
      unique: true,
      terms: [{ field: ["data", "id"] }],
    },
  },
  privilege: {
    actions: {
      read: true,
    },
  },
};

export const TemporaryLogin = {
  name: "TemporaryLogin",
  indexes: {},
  privilege: {
    actions: { read: true },
  },
};

export const CreatePersonFunction = {
  name: "CreatePerson",
  call: (person: { id: string }) => q.Call("CreatePerson", person),
  role: "server",
  body: q.Query(
    q.Lambda((person) =>
      q.Create(q.Collection(People.name), {
        data: {
          id: q.Select("id", person),
          homepage: q.Select(["ref"], CreateEntityFunction.call({})),
        },
      })
    )
  ),
  privilege: {
    call: false,
  },
} as const;
