import { Block } from "./fauna/queries/block";

export function uuidv4(): string {
  //@ts-ignore
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
    (
      c ^
      (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
    ).toString(16)
  );
}

// TODO Refactor this somehow...
export const blockID = (b: Block) => {
  if (b.tempID) return b.tempID;
  if (!b.blockEntity?.id) throw new Error("Block has no id");
  return b.blockEntity.id;
};
export function extractLinks(value: string) {
  return [...value.matchAll(/\[\[([^\[\n\]]*)\]\]/g)];
}
type Token = {
  type: "plain" | "link";
  value: string;
  position: number;
};

export function parser(input: string) {
  let tokens: Token[] = [];
  let lastTokenEnd = 0;
  let position = 0;
  while (position < input.length) {
    if (input[position] === "[" && input[position + 1] === "[") {
      let end = /\]\]/.exec(input.slice(position));
      if (!end) {
        tokens.push({
          type: "plain",
          value: input.slice(lastTokenEnd),
          position: lastTokenEnd,
        });
        position = input.length;
        lastTokenEnd = input.length;
        break;
      }
      tokens.push({
        type: "plain",
        value: input.slice(lastTokenEnd, position),
        position: lastTokenEnd,
      });
      tokens.push({
        type: "link",
        value: input.slice(position, position + end.index + 2),
        position,
      });
      lastTokenEnd = position + end.index + 2;
    }
    position++;
  }
  if (lastTokenEnd < input.length)
    tokens.push({
      type: "plain",
      value: input.slice(lastTokenEnd),
      position: lastTokenEnd,
    });
  return tokens;
}

export function getLinkAtCursor(text: string, cursor: number) {
  let start: number | undefined;
  let end: number | undefined;
  for (let i = 0; i < 140; i++) {
    let startPosition = cursor - i;
    if (!start && text.slice(startPosition - 2, startPosition) === "[[")
      start = startPosition;
    if (!end && text.slice(cursor + i, cursor + i + 2) === "]]")
      end = cursor + i;
  }
  if (!start || start < 0 || !end) return undefined;
  return {
    value: text.slice(start, end),
    start,
    end,
  };
}

export async function computeHash(data: ArrayBuffer): Promise<string> {
  const buf = await crypto.subtle.digest("SHA-256", data);
  return Array.from(new Uint8Array(buf), (b) =>
    b.toString(16).padStart(2, "0")
  ).join("");
}
