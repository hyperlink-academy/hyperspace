import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import { useAuthentication } from "src/auth";
import { Separator } from "src/components/Layout";
import { SearchBar } from "./SearchBar";
import { LogoIcon, Home } from "src/components/Icons";

export function AppHeader() {
  let { data: auth } = useAuthentication();
  if (!auth || auth.temp)
    return (
      <div
        style={{
          display: "grid",
          gridTemplateColumns: "max-content max-content max-content auto",
          gridGap: "1rem",
          alignItems: "center",
          height: "2.6rem",
        }}
      >
        <Link href="/">
          <a className="text-grey55 hover:text-textLink">{LogoIcon}</a>
        </Link>
        <Separator />
        <LoginLogout />
        <SearchBar />
      </div>
    );
  //HEADER IF LOGGED IN
  else
    return (
      <div
        style={{
          display: "grid",
          gridTemplateColumns: "max-content max-content auto",
          gridGap: "1rem",
          alignItems: "center",
          height: "2.6rem",
        }}
      >
        <Link href="/">
          <a className="text-grey55 hover:text-textLink">{LogoIcon}</a>
        </Link>
        <Link href="/home">
          <a className="text-grey55 hover:text-textLink">{Home}</a>
        </Link>
        {/* <Separator /> */}
        {/* <CreateNew /> */}
        <SearchBar />
      </div>
    );
}

function LoginLogout() {
  let { data: auth, mutate } = useAuthentication();
  let router = useRouter();
  let redirect =
    router.pathname === "/"
      ? ""
      : "?redirect=" + encodeURIComponent(router.asPath);
  const logout = async () => {
    mutate(undefined, false);
    await fetch("/api/logout");
    mutate();
  };

  if (!auth || auth.temp)
    return (
      <a
        href={"/api/login" + redirect}
        className="text-textSecondary font-bold"
      >
        login
      </a>
    );
  else
    return (
      <div className="text-textSecondary font-bold">
        <button onClick={logout}>logout</button>
      </div>
    );
}
