import { values } from "faunadb";
import {
  SyntheticEvent,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";

import { blockID, computeHash, getLinkAtCursor } from "src/utils";
import Textarea from "src/components/Textarea";
import { moveBlockFunctionContext } from "src/KeyboardSensor";
import { Block, updateContentQuery } from "src/fauna/queries/block";
import { getCoordinatesInTextarea } from "src/getCoordinatesInTextarea";
import { Autocomplete, useSuggestions } from "src/components/Autocomplete";
import { LinkNameToIdProvider } from "src/components/EntityLink";
import { BlockContainer } from "src/components/Block/Container";

import { useDebouncedEffect } from "src/hooks/useDebounced";
import { useMutableTextarea } from "src/hooks/useCollaborativeDoc";
import { useEntity } from "src/hooks/useEntity";
import { useFauna } from "src/hooks/useFauna";
import { CreateOrUpdateFact } from "src/fauna/setup/Facts";
import { useAuthentication } from "src/auth";

type BlockType = "default" | "h1" | "h2" | "h3";
const BlockTypes: { [t in BlockType]: { icon: string; styles: string } } = {
  default: { icon: "", styles: "" },
  h1: { icon: "#", styles: "text-2xl font-bold" },
  h2: { icon: "##", styles: "text-xl font-bold" },
  h3: { icon: "###", styles: "text-base font-bold" },
};

export function TextBlock(props: {
  block: Block;
  index: number;
  send: (x: object) => void;
  mutate: ReturnType<typeof useEntity>["mutate"];
  createBlock: ReturnType<typeof useEntity>["createBlock"];
  deleteBlock: () => void;
  setFocusedBlock: (i: string | null) => void;
  focusedBlock: string | null;
  blocks: Block[];
}) {
  let textarea = useRef<HTMLTextAreaElement | null>(null);
  let { data: user } = useAuthentication();
  let fauna = useFauna();
  let [value, onChange, transact] = useMutableTextarea(
    textarea,
    props.block.contentText?.value || ""
  );
  let [blockType, setBlockType] = useState<BlockType>(
    !props.block.blockType
      ? "default"
      : (props.block.blockType.data.value as BlockType)
  );

  useDebouncedEffect(
    async () => {
      if (!fauna || !props.block.blockEntity) return;
      await fauna.query(
        CreateOrUpdateFact(props.block.blockEntity, "text/type", blockType)
      );
    },
    400,
    [blockType]
  );
  useDebouncedEffect(
    async () => {
      if (
        !props.block.contentText?.factRef ||
        !props.block.blockEntity ||
        !fauna
      )
        return;
      if (value === props.block.contentText.value) return;
      console.log("mutating text");
      await fauna.query(
        updateContentQuery(
          props.block.contentText.factRef,
          value,
          props.block.blockEntity
        )
      );
      props.send({ type: "mutate" });
    },
    400,
    [value]
  );

  let [cursorCoordinates, setCursorCoordinates] =
    useState<undefined | { top: number; left: number; textIndex: number }>();
  let {
    suggestions,
    suggestionIndex,
    setSuggestionIndex,
    close,
    setSuggestionPrefix,
    suggestionPrefix,
  } = useSuggestions();
  let { moveBlock } = useContext(moveBlockFunctionContext);
  useEffect(() => {
    if (
      props.focusedBlock !== null &&
      props.focusedBlock === blockID(props.block) &&
      textarea.current
    )
      textarea.current.focus();
  }, [props.focusedBlock, props.block]);

  const onKeyDown = useOnKeyDownCallback({
    suggestions,
    close,
    cursorCoordinates,
    blockType,
    setBlockType,
    transact,
    moveBlock,
    suggestionPrefix,
    blockIndex: props.index,
    block: props.block,
    blocks: props.blocks,
    suggestionIndex,
    setSuggestionIndex,
    setFocusedBlock: props.setFocusedBlock,
    deleteBlock: props.deleteBlock,
    createBlock: props.createBlock,
  });

  const onSelect = useCallback(
    (e: SyntheticEvent<HTMLTextAreaElement>) => {
      let value = e.currentTarget.value,
        start = e.currentTarget.selectionStart,
        end = e.currentTarget.selectionEnd;
      if (start !== end) return setCursorCoordinates(undefined);

      let link = getLinkAtCursor(value, start);
      setSuggestionPrefix(link?.value);
      if (!link) {
        setCursorCoordinates(undefined);
        close();
        return;
      }
      let coordinates = getCoordinatesInTextarea(e.currentTarget, link.start);
      let textareaPosition = e.currentTarget.getBoundingClientRect();
      setCursorCoordinates({
        textIndex: link.start,
        top:
          coordinates.top +
          textareaPosition.top +
          document.documentElement.scrollTop +
          coordinates.height,
        left: coordinates.left + textareaPosition.left,
      });
    },
    [setCursorCoordinates, setSuggestionPrefix]
  );

  const onPaste = useCallback(
    async (e: React.ClipboardEvent<HTMLTextAreaElement>) => {
      let items = e.clipboardData.items;
      for (let i = 0; i < items.length; i++) {
        if (items[i].type.includes("image")) {
          let image = items[i].getAsFile();
          if (!image) return;
          let hash = await computeHash(await image.arrayBuffer());
          let res = await fetch("/api/upload-image", {
            method: "POST",
            body: hash,
          });
          let { url: uploadUrl, imageID, exists } = await res.json();
          if (!exists)
            await fetch(uploadUrl, {
              method: "PUT",
              headers: {
                "Content-Type": "image",
                "x-amz-acl": "public-read",
              },
              body: image,
            });
          props.createBlock({
            index: props.index,
            image: `https://hyperspace-uploads.nyc3.digitaloceanspaces.com/${imageID}`,
          });
        }
      }
    },
    []
  );

  return (
    <>
      {suggestionPrefix &&
      cursorCoordinates &&
      suggestions.length > 0 &&
      props.focusedBlock === blockID(props.block) ? (
        <Autocomplete
          top={cursorCoordinates.top}
          onClick={(item) => {
            transact((tx) => {
              if (!cursorCoordinates || !suggestionPrefix) return;
              tx.delete(cursorCoordinates.textIndex, suggestionPrefix.length);
              tx.insert(cursorCoordinates.textIndex, item);
            }, item.length + 2);
            close();
          }}
          left={cursorCoordinates.left}
          selected={suggestionIndex}
          suggestions={suggestions.map((s) => s.title)}
          suggestionPrefix={suggestionPrefix}
        />
      ) : null}

      <LinkNameToIdProvider
        namesToIds={props.block.links.reduce((acc, link) => {
          if (link) acc[link.title] = link.entity.id;
          return acc;
        }, {} as { [k: string]: string })}
      >
        <div className="grid grid-flow-row grid-cols-[min-content,auto] gap-2 items-center">
          <div
            className={`w-8 text-right justify-self-end text-grey80 font-normal ${BlockTypes[blockType].styles}`}
          >
            {BlockTypes[blockType].icon}
          </div>
          <BlockContainer
            isDragDisabled={!user || user.temp}
            id={blockID(props.block)}
            index={props.index}
            onDelete={props.deleteBlock}
          >
            <Textarea
              previewOnly={user?.temp}
              className={`bg-backgroundApp focus:outline-none ${BlockTypes[blockType].styles}`}
              focused={props.focusedBlock === blockID(props.block)}
              autoFocus={props.focusedBlock === blockID(props.block)}
              ref={textarea}
              value={value}
              onSelect={onSelect}
              onPaste={onPaste}
              onBlur={(_e) => {
                if (props.block.contentText?.value !== value) props.mutate();
                close();
              }}
              onFocus={(_e) => {
                props.setFocusedBlock(blockID(props.block));
              }}
              onChange={(e) => {
                onChange(e);
                let value = e.currentTarget.value,
                  start = e.currentTarget.selectionStart,
                  end = e.currentTarget.selectionEnd;
                if (start !== end) return setCursorCoordinates(undefined);

                let link = getLinkAtCursor(value, start);
                setSuggestionPrefix(link?.value);
                if (!link) {
                  setCursorCoordinates(undefined);
                  close();
                  return;
                }
                let coordinates = getCoordinatesInTextarea(
                  e.currentTarget,
                  link.start
                );

                let textareaPosition = e.currentTarget.getBoundingClientRect();
                setCursorCoordinates({
                  textIndex: link.start,
                  top:
                    coordinates.top +
                    textareaPosition.top +
                    document.documentElement.scrollTop +
                    coordinates.height,
                  left: coordinates.left + textareaPosition.left,
                });
              }}
              onKeyDown={onKeyDown}
            />
          </BlockContainer>
        </div>
      </LinkNameToIdProvider>
    </>
  );
}

const useOnKeyDownCallback = (deps: {
  suggestions: { title: string; ref: values.Ref }[];
  close: () => void;
  cursorCoordinates?: { textIndex: number };
  moveBlock?: (item: string, direction: "up" | "down") => void;
  suggestionPrefix?: string;
  blockIndex: number;
  blockType: BlockType;
  setBlockType: (type: BlockType) => void;
  block: Block;
  blocks: Block[];
  transact: ReturnType<typeof useMutableTextarea>[2];
  suggestionIndex: number;
  setSuggestionIndex: (x: number | ((x: number) => number)) => void;
  setFocusedBlock: (i: string | null) => void;
  deleteBlock: () => void;
  createBlock: ReturnType<typeof useEntity>["createBlock"];
}) => {
  let {
    deleteBlock,
    block,
    blocks,
    suggestions,
    transact,
    blockType,
    setBlockType,
    setSuggestionIndex,
    cursorCoordinates,
    moveBlock,
    createBlock,
    blockIndex,
    close,
    setFocusedBlock,
    suggestionIndex,
    suggestionPrefix,
  } = deps;
  return useCallback(
    (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
      let value = e.currentTarget.value,
        start = e.currentTarget.selectionStart,
        end = e.currentTarget.selectionEnd;

      switch (e.key) {
        case "#": {
          if (start !== 0) break;
          e.preventDefault();
          if (blockType === "default") {
            setBlockType("h1");
          }
          if (blockType === "h1") {
            setBlockType("h2");
          }
          if (blockType === "h2") {
            setBlockType("h3");
          }
          break;
        }
        case "Escape": {
          if (suggestions.length > 0 && !!cursorCoordinates) {
            e.preventDefault();
            close();
          } else setFocusedBlock(null);
          break;
        }
        case "Enter": {
          if (e.ctrlKey || e.metaKey) {
            e.preventDefault();
            createBlock({
              index: blockIndex,
              afterTemp: (tempID: string) => setFocusedBlock(tempID),
            });
            break;
          }
          if (suggestions.length > 0 && !!cursorCoordinates) {
            e.preventDefault();
            let value = suggestions[suggestionIndex];
            if (!value) break;
            transact((text) => {
              if (!cursorCoordinates || !suggestionPrefix) return;
              text.delete(cursorCoordinates.textIndex, suggestionPrefix.length);
              text.insert(cursorCoordinates.textIndex, value.title);
            }, value.title.length + 2);
            close();
            break;
          }
          break;
        }
        case "ArrowUp": {
          if (e.ctrlKey) {
            e.preventDefault();
            if (e.shiftKey) {
              if (moveBlock) moveBlock(blockID(block), "up");
              return;
            }
            if (blockIndex === 0) return;
            setFocusedBlock(blockID(blocks[blockIndex - 1]));
            break;
          }
          if (suggestions.length > 0 && !!cursorCoordinates) {
            e.preventDefault();
            if (suggestionIndex > 0) setSuggestionIndex((i) => i - 1);
            break;
          }
          break;
        }
        case "ArrowDown": {
          if (e.ctrlKey) {
            e.preventDefault();
            if (e.shiftKey) {
              if (moveBlock) moveBlock(blockID(block), "down");
              return;
            }
            if (blockIndex === blocks.length - 1) return;
            setFocusedBlock(blockID(blocks[blockIndex + 1]));
            break;
          }
          if (suggestions.length > 0 && !!cursorCoordinates) {
            e.preventDefault();
            if (suggestionIndex < suggestions.length - 1)
              setSuggestionIndex((i) => i + 1);
            break;
          }
          break;
        }
        case "Backspace": {
          if (start === 0 && end === 0) {
            if (blockType === "h1") setBlockType("default");
            if (blockType === "h2") setBlockType("h1");
            if (blockType === "h3") setBlockType("h2");
            if (blockType === "default") {
              if (e.currentTarget.value === "") {
                e.preventDefault();
                deleteBlock();
                if (blockIndex === 0) {
                  if (blocks[1]) setFocusedBlock(blockID(blocks[1]));
                  else setFocusedBlock(null);
                } else setFocusedBlock(blockID(blocks[blockIndex - 1]));
                break;
              }
            }
            break;
          }

          if (
            value[start - 1] === "[" &&
            value[start] === "]" &&
            start === end
          ) {
            e.preventDefault();
            transact((text) => {
              text.delete(start - 1, 2);
            });
            break;
          }

          if (
            value[start - 2] === "[" &&
            value[start - 1] === "]" &&
            start === end
          ) {
            e.preventDefault();
            transact((text) => {
              text.delete(start - 2, 2);
            });
            break;
          }

          break;
        }
        case "[": {
          if (e.ctrlKey || e.altKey) break;
          e.preventDefault();
          if (start !== end) {
            transact((text) => {
              text.insert(start, "[");
              text.insert(end + 1, "]");
            });
            //React seems to change the selection state if you set the value to something the current value is not
          } else {
            transact((text) => {
              text.insert(start, "[]");
            }, 1);
          }
          break;
        }
        case "]": {
          if (e.ctrlKey || e.altKey) break;
          let start = e.currentTarget.selectionStart,
            end = e.currentTarget.selectionEnd;
          if (start === end) {
            if (e.currentTarget.value[start] === "]") {
              e.preventDefault();
              e.currentTarget.setSelectionRange(start + 1, start + 1);
            }
          }
          break;
        }
      }
    },
    [
      deleteBlock,
      block,
      blocks,
      suggestions,
      setSuggestionIndex,
      cursorCoordinates,
      transact,
      moveBlock,
      createBlock,
      blockIndex,
      close,
      setFocusedBlock,
      suggestionIndex,
      suggestionPrefix,
    ]
  );
};
