import { Block } from "src/fauna/queries/block";
import { BlockContainer } from "src/components/Block/Container";
import { EntityLink } from "src/components/EntityLink";
import { useAuthentication } from "src/auth";

export const LinkBlock = (props: {
  block: Block;
  id: string;
  index: number;
  deleteBlock: () => void;
}) => {
  let title = props.block.links[0].title;
  let ref = props.block.links[0].entity;
  let icon = props.block.links[0].icon;
  let { data: user } = useAuthentication();
  return (
    <BlockContainer
      id={props.id}
      isDragDisabled={!user || user.temp}
      index={props.index}
      onDelete={props.deleteBlock}
      className="ml-12 !border-borderDefault"
    >
      <div className="grid grid gap-4 grid-cols-[32px,auto] items-center">
        <img src={icon} />
        <EntityLink entityName={title} entityRef={ref}>
          <h2>{title}</h2>
        </EntityLink>
      </div>
    </BlockContainer>
  );
};
