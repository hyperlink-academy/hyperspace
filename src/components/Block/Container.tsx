import { Draggable } from "react-beautiful-dnd";
import { DeleteIcon } from "src/components/Icons";

export const BlockContainer: React.FC<{
  id: string;
  index: number;
  isDragDisabled?: boolean;
  onDelete: () => void;
  className?: string;
}> = (props) => {
  return (
    <Draggable
      draggableId={props.id}
      index={props.index}
      isDragDisabled={props.isDragDisabled}
    >
      {(provided, snapshot) => {
        return (
          <div
            {...provided.draggableProps}
            ref={provided.innerRef}
            className={`
            ${props.isDragDisabled ? "" : "blockWrapper"}
            grid grid-flow-row grid-cols-[100%,1.5rem] max-w-full`}
          >
            <style jsx>{`
              .dragGripper {
                background-image: url("../img/dragGripper.svg");
                background-repeat: repeat-y;
              }

              .dragGripper {
                ${snapshot.isDragging ? "opacity: 1" : "opacity: 0"};
              }

              .blockWrapper:hover .dragGripper,
              .blockWrapper:hover .deleteButton {
                opacity: 0.4;
              }
              .blockWrapper:hover .blockContent {
                @apply border-grey80;
                @apply border;
              }

              .dragGripper:hover,
              .deleteButton:hover {
                opacity: 1 !important;
              }

              .deleteButton {
                ${snapshot.isDragging ? "opacity: .4" : "opacity: 0"};
              }

              .deleteButton:hover {
                @apply text-accentRed;
              }
            `}</style>
            <div
              className={`
                ${props.isDragDisabled ? "" : "blockContent"}
              ${
                props.isDragDisabled ? "" : "focus-within:!border-borderDefault"
              }
              ${props.className}
              ${snapshot.isDragging ? "shadow-lg" : ""}
              py-2 px-3
              grid gap-2 grid-cols-[auto,max-content] 
              items-end 
              border rounded-sm border-backgroundApp`}
            >
              {props.children}
              {props.isDragDisabled ? null : (
                <button
                  onClick={() => props.onDelete()}
                  className={`deleteButton text-textPrimary mb-0.5`}
                >
                  <div>{DeleteIcon}</div>
                </button>
              )}
            </div>
            <div
              style={{
                height: "100%",
                padding: ".25rem 0 0 .5rem",
              }}
            >
              {/* DRAG GRIPPER */}
              {props.isDragDisabled ? null : (
                <div
                  className="dragGripper w-4 h-full rounded-md"
                  {...provided.dragHandleProps}
                />
              )}
            </div>
          </div>
        );
      }}
    </Draggable>
  );
};
