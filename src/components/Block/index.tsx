import { blockID } from "src/utils";
import { BlockContainer } from "src/components/Block/Container";
import { TextBlock } from "src/components/Block/TextBlock";
import { LinkBlock } from "src/components/Block/LinkBlock";
import { Block } from "src/fauna/queries/block";
import { useEntity } from "src/hooks/useEntity";
import { CreateBlockButtonInline } from "src/components/CreateBlock";
import { useAuthentication } from "src/auth";

export function BlockComponent(props: {
  block: Block;
  index: number;
  send: (x: object) => void;
  mutate: ReturnType<typeof useEntity>["mutate"];
  createBlock: ReturnType<typeof useEntity>["createBlock"];
  deleteBlock: () => void;
  setFocusedBlock: (i: string | null) => void;
  focusedBlock: string | null;
  blocks: Block[];
}) {
  let block;
  let { data: user } = useAuthentication();
  switch (true) {
    case !!props.block.contentImage: {
      block = (
        <BlockContainer
          isDragDisabled={!user || user.temp}
          id={blockID(props.block)}
          index={props.index}
          onDelete={props.deleteBlock}
        >
          <div>
            <img
              className="mx-auto p-3"
              src={props.block.contentImage?.value}
            />
          </div>
        </BlockContainer>
      );
      break;
    }
    case props.block.contentText &&
      /^\[\[[^\[\n\]]*\]\]$/.test(props.block.contentText.value) &&
      !!props.block.links[0]: {
      block = (
        <LinkBlock
          block={props.block}
          id={blockID(props.block)}
          index={props.index}
          deleteBlock={props.deleteBlock}
        />
      );
      break;
    }
    default: {
      block = <TextBlock {...props} />;
    }
  }

  return (
    <>
      {!user || user.temp ? (
        <div className="min-h-[1.25rem]" />
      ) : (
        <CreateBlockButtonInline
          onClick={() =>
            props.createBlock({
              index: props.index - 1,
              afterTemp: (tempID: string) => props.setFocusedBlock(tempID),
            })
          }
        />
      )}
      {block}
    </>
  );
}
