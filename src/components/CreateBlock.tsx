import { Tertiary } from "src/components/Buttons";
export function CreateBlockButtonInline(props: {
  onClick: (e?: React.MouseEvent) => void;
}) {
  return (
    <div className="createBlockWrapper grid items-center">
      <button
        className="createBlockButton min-h-[1.25rem] px-3"
        onClick={props.onClick}
      >
        <div
          className={`createBlockButtonContent pointer-events-none w-full items-center text-textSecondary hidden `}
        >
          <hr className="text-grey80" />
          <p className="text-xs mx-4 ">new block!</p>
          <hr className="text-grey80" />
        </div>
      </button>

      <style jsx>
        {`
          .createBlockWrapper:hover .createBlockButtonContent {
            visibility: visible;
            display: grid;
            grid-template-columns: auto max-content auto;
          }
        `}
      </style>
    </div>
  );
}

export function CreateBlockButton(props: {
  onClick: (e?: React.MouseEvent) => void;
}) {
  return (
    <Tertiary onClick={props.onClick}>
      <p className="text-sm">add a new block</p>
    </Tertiary>
  );
}
