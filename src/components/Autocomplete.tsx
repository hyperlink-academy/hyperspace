import { values, query as q } from "faunadb";
import { useEffect, useRef, useState } from "react";
import { Facts } from "src/fauna/setup/Facts";
import { useDebouncedEffect } from "src/hooks/useDebounced";
import { useFauna } from "src/hooks/useFauna";

export const Autocomplete = (props: {
  top: number;
  left: number;
  selected: number;
  suggestions: string[];
  onClick: (item: string) => void;
  suggestionPrefix: string;
}) => {
  const previousSelected = useRef(0);
  useEffect(() => {
    previousSelected.current === props.selected;
  });
  return (
    <div
      style={{ top: props.top, left: props.left - 16, position: "absolute" }}
      className="bg-backgroundWhite border border-borderDefault text-textSecondary rounded-sm py-1 w-64 max-h-32 overflow-y-scroll z-10"
    >
      <ul>
        <li
          key={"create"}
          className={`px-2 py-1 italic text-textSecondary hover:bg-backgroundBlue}`}
        >
          Create &quot;{props.suggestionPrefix}&quot;
        </li>

        {props.suggestions.map((result, index) => {
          return (
            <ListItem
              key={result}
              onClick={props.onClick}
              previousSelectedIndex={previousSelected.current}
              index={index}
              selectedIndex={props.selected}
              value={result}
            />
          );
        })}
      </ul>
    </div>
  );
};

const ListItem = (props: {
  value: string;
  index: number;
  onClick: (item: string) => void;
  previousSelectedIndex: number;
  selectedIndex: number;
}) => {
  let el = useRef<HTMLLIElement>(null);
  useEffect(() => {
    if (el.current && props.selectedIndex === props.index) {
      el.current.scrollIntoView({
        block: "nearest",
      });
    }
  }, [el, props.index, props.selectedIndex]);
  return (
    <>
      <style jsx>
        {`
          .autocomplete-hover:hover {
            @apply bg-backgroundBlue;
            cursor: pointer;
          }
        `}
      </style>
      <li
        ref={el}
        onMouseDown={(e) => {
          e.preventDefault();
          props.onClick(props.value);
        }}
        className={`autocomplete-hover py-1 px-2 ${
          props.index === props.selectedIndex ? "bg-backgroundBlue" : ""
        }`}
      >
        {props.value}
      </li>
    </>
  );
};

export const useSuggestions = () => {
  let fauna = useFauna();
  let [suggestions, setSuggestions] = useState<
    { title: string; ref: values.Ref }[]
  >([]);
  let [suggestionPrefix, setSuggestionPrefix] = useState<undefined | string>();
  let [suggestionIndex, setSuggestionIndex] = useState(0);
  useEffect(() => {
    if (suggestionIndex > suggestions.length - 1)
      setSuggestionIndex(suggestions.length - 1);
  }, [suggestionIndex, suggestions]);
  useDebouncedEffect(
    async () => {
      if (!fauna) return;
      if (!suggestionPrefix) return setSuggestions([]);
      let data: { title: string; ref: values.Ref }[] = await fauna.query(
        q.Distinct(
          q.Map(
            q.Select(
              "data",
              q.Paginate(
                q.Range(
                  q.Match(q.Index(Facts.indexes.search.name)),
                  q.Casefold(suggestionPrefix),
                  q.Casefold(suggestionPrefix + String.fromCharCode(65535))
                )
              )
            ),
            q.Lambda((x) =>
              q.Object({ title: q.Select(1, x), ref: q.Select(2, x) })
            )
          )
        )
      );
      setSuggestions(data);
    },
    200,
    [suggestionPrefix]
  );
  return {
    suggestions: suggestions.filter((x) =>
      suggestionPrefix
        ? x.title
            .toLocaleLowerCase()
            .includes(suggestionPrefix.toLocaleLowerCase())
        : true
    ),
    suggestionIndex,
    setSuggestionIndex,
    suggestionPrefix,
    setSuggestionPrefix,
    close() {
      setSuggestions([]);
    },
  };
};
