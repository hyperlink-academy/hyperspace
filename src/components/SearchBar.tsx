import { useFauna } from "src/hooks/useFauna";
import { Client, values } from "faunadb";
import { useEffect, useRef } from "react";
import { useDebouncedKey } from "src/hooks/useDebounced";
import { useCombobox } from "downshift";
import { useRouter } from "next/router";
import { mutate } from "swr";
import { CreateNewEntity, Entity, entityQuery } from "src/fauna/queries/entity";
import { useSuggestions } from "./Autocomplete";
import { useAuthentication } from "src/auth";

type SearchResults = (
  | { title: string; ref: values.Ref; type: "searchResult" }
  | { type: "createButton" }
)[];

export function SearchBar() {
  let fauna = useFauna();
  let router = useRouter();
  let { data: auth } = useAuthentication();
  let {
    suggestions: results,
    close,
    setSuggestionPrefix,
    suggestionPrefix,
  } = useSuggestions();
  let callWithDebounce = useDebouncedKey();
  let inputEl = useRef<HTMLInputElement | null>(null);
  useEffect(() => {
    let listener = (e: KeyboardEvent) => {
      if (!inputEl.current) return;
      if ((e.ctrlKey || e.altKey) && e.key === "k") {
        e.preventDefault();
        inputEl.current.focus();
      }
    };
    window.addEventListener("keydown", listener);
    return () => {
      window.removeEventListener("keydown", listener);
    };
  }, [inputEl]);

  let resultsWithCreate = [
    ...results.map((item) => {
      return {
        type: "searchResult" as const,
        ...item,
      };
    }),
  ] as SearchResults;

  if (
    !results.find(
      (x) =>
        x.title.toLocaleLowerCase() === suggestionPrefix?.toLocaleLowerCase()
    ) &&
    !auth?.temp
  )
    resultsWithCreate = resultsWithCreate.concat([
      { type: "createButton" as const },
    ]);

  const createNewDocument = async (title: string) => {
    if (!fauna) return;
    let newEntity: Entity = await fauna.query(
      CreateNewEntity({ "content/title": title })
    );
    mutate(newEntity.ref.id, newEntity);
    router.push("/" + newEntity.ref.id);
  };

  let {
    isOpen,
    highlightedIndex,
    getInputProps,
    getComboboxProps,
    getMenuProps,
    setInputValue,
    inputValue,
    getItemProps,
  } = useCombobox({
    defaultHighlightedIndex: 0,
    items: resultsWithCreate,
    itemToString: (item) =>
      !item ? "" : item.type === "createButton" ? "createButton" : item.title,
    onHighlightedIndexChange: (change) => {
      if (change.highlightedIndex === undefined) return;
      callWithDebounce(
        "highlightChanged",
        () => {
          if (change.highlightedIndex === undefined || !fauna) return;
          let entityRef = results[change.highlightedIndex]?.ref;
          if (!entityRef) return;
          prefetchEntity(entityRef, fauna);
        },
        250
      );
    },
    onSelectedItemChange: (change) => {
      switch (change.type) {
        case useCombobox.stateChangeTypes.InputKeyDownEnter:
        case useCombobox.stateChangeTypes.ItemClick: {
          if (!change.selectedItem) return;
          if (change.selectedItem.type === "searchResult") {
            router.push("/" + change.selectedItem.ref.id);
            setInputValue("");
            close();
            return;
          }
          setInputValue("");
          createNewDocument(inputValue);
        }
      }
    },
    onInputValueChange: ({ inputValue }) => {
      setSuggestionPrefix(inputValue);
      if (!inputValue) close();
    },
  });
  return (
    <div className=" self-start justify-self-end max-w-md w-full">
      {/* SEARCH INPUT BOX */}
      <div {...getComboboxProps()} className="z-10">
        <input
          {...getInputProps({
            ref: inputEl,
            onKeyDown: (e) => {
              if (e.key === "Escape") e.currentTarget.blur();
            },
          })}
          className="
            bg-backgroundWhite 
            px-4 
            py-2 
            border 
            border-borderDefault 
            rounded-sm 
            w-full 
            self-end 
            relative"
          placeholder={auth?.temp ? "Search" : "Search or Create"}
        />
      </div>

      {/* DROP DOWN SEARCH RESULTS */}
      <ul
        {...getMenuProps()}
        className="
          box-border 
          py-2 
          mt-1
          max-w-md 
          w-full 
          max-h-64 
          overflow-y-auto 
          bg-backgroundWhite  
          border 
          border-borderDefault 
          rounded-sm 
          absolute
          z-10"
        style={{
          display: isOpen && inputValue != "" ? undefined : "none",
        }}
      >
        {/* ITEM IN DROP DOWN */}
        {!isOpen
          ? null
          : resultsWithCreate.map((item, index) => {
              let selected = index === highlightedIndex;
              return (
                <li
                  key={
                    item.type === "createButton"
                      ? "create-button" + index
                      : item.title + index
                  }
                  className={`px-3 py-2 w-full cursor-pointer
                              ${selected ? "bg-backgroundBlue" : ""}
                              ${
                                item.type === "createButton"
                                  ? "text-textSecondary italic"
                                  : ""
                              }
                  `}
                  {...getItemProps({ item, index })}
                >
                  {item.type === "createButton"
                    ? `Create "${inputValue}"`
                    : item.title}
                </li>
              );
            })}
      </ul>
    </div>
  );
}

const prefetchEntity = async (entityRef: values.Ref, fauna: Client) => {
  let entity: Entity = await fauna.query(entityQuery(entityRef));
  return mutate(entityRef.id, (currentEntity?: Entity) => {
    if (currentEntity) return currentEntity;
    return entity;
  });
};
