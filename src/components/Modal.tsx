export const Modal: React.FC<{ open: boolean; onClose: () => void }> = (
  props
) => {
  if (!props.open) return null;
  return (
    <>
      <style jsx>
        {`
          .modal {
            position: fixed;
            border: 2px solid;
            padding: 16px 24px;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            z-index: 10;
            justify-content: center;
            text-align: left;
            @apply border-borderDefault;
            @apply bg-backgroundBlue;
          }
          .backgroundBlur {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: white;
            opacity: 0.75;
            z-index: 9;
          }
          .backgroundBlur @supports (backdrop-filter: blur(2.5px)) {
            backdrop-filter: blur(2.5px);
            background-color: transparent;
            opacity: 1;
          }
        `}
      </style>
      <button className="backgroundBlur" onClick={props.onClose} />
      <div className="modal">{props.children}</div>
    </>
  );
};
