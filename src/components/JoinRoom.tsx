import Daily, {
  DailyCall,
  DailyEventObjectAppMessage,
  DailyParticipant,
} from "@daily-co/daily-js";
import {
  createContext,
  useContext,
  useEffect,
  useRef,
  useState,
  useCallback,
} from "react";
import Router from "next/router";
import { useAuthentication } from "src/auth";
import { useEntity } from "src/hooks/useEntity";
import { Secondary, SecondaryDestructive } from "src/components/Buttons";

let events = [
  "loading",
  "load-attempt-failed",
  "loaded",
  "started-camera",
  "camera-error",
  "joining-meeting",
  "joined-meeting",
  "left-meeting",
  "participant-joined",
  "participant-updated",
  "participant-left",
  "track-started",
  "track-stopped",
  "recording-started",
  "recording-stopped",
  "recording-stats",
  "recording-error",
  "recording-upload-completed",
  "recording-data",
  "app-message",
  "local-screen-share-started",
  "local-screen-share-stopped",
  "active-speaker-change",
  "active-speaker-mode-change",
  "network-quality-change",
  "network-connection",
  "fullscreen",
  "exited-fullscreen",
  "error",
  "click",
  "mousedown",
  "mouseup",
  "mouseover",
  "mousemove",
  "touchstart",
  "touchmove",
  "touchend",
  "live-streaming-started",
  "live-streaming-stopped",
  "live-streaming-error",
  "access-state-updated",
  "waiting-participant-added",
  "waiting-participant-updated",
  "waiting-participant-removed",
] as const;

export let CallContext = createContext({
  joinCall: (_room: { name: string; id: string }) => new Promise(() => {}),
  muted: false,
  videoOn: false,
  setLocalAudio: (_mute: boolean) => {},
  setLocalVideo: (_video: boolean) => {},
  setInputDevices: (_devices: {
    audioDeviceId?: string;
    videoDeviceId?: string;
  }) => {},
  leaveCall: () => {},
  call: null as { name: string; id: string } | null,
  participants: [] as DailyParticipant[],
  devices: [] as MediaDeviceInfo[],
  sendMessage: (_msg: Message) => {},
});

type Message = { type: "summon"; page: string };

export const CallProvider: React.FC = (props) => {
  let [room, setRoom] = useState<DailyCall | null>(null);
  let [call, setInCall] = useState<null | { name: string; id: string }>(null);
  let [participants, setParticipants] = useState<DailyParticipant[]>([]);
  let { data: user } = useAuthentication();
  let { entity } = useEntity();
  useEffect(() => {
    if (!call || !room || !entity?.title?.value || !user) return;
    room.setUserName(
      JSON.stringify({
        username: user.display_name || user.username,
        page: {
          title: entity.title.value,
          ref: entity.ref.id,
        },
      })
    );
  }, [entity?.title?.value, entity?.ref?.id, room, call, user]);
  let devices = useDevices();
  useEffect(() => {
    if (!room) return;
    let currentRoom = room;
    let updateParticipants = () => {
      setParticipants(Object.values(currentRoom.participants()));
    };
    const handleAppMessage = (msg?: DailyEventObjectAppMessage) => {
      if (!msg) return;
      let data = msg.data as Message;
      switch (data.type) {
        case "summon": {
          console.log("summoning");
          Router.push("/" + data.page);
        }
      }
    };
    currentRoom.on("app-message", handleAppMessage);
    for (let event of events) {
      currentRoom.on(event, updateParticipants);
    }
    return () => {
      currentRoom.off("app-message", handleAppMessage);
      for (let event of events) {
        currentRoom.off(event, updateParticipants);
      }
    };
  }, [room]);

  const joinCall = useCallback(
    async (roomData: { name: string; id: string }) => {
      if (room) await room.leave();
      await fetch("/api/get-room-token", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ room: roomData.id }),
      });
      const call =
        room ||
        Daily.createCallObject({
          //@ts-ignore
          audioSource: true, // start with audio on to get mic permission from user at start
          //@ts-ignore
          videoSource: false,
          dailyConfig: {
            experimentalChromeVideoMuteLightOff: true,
          },
        });
      setRoom(call);
      console.log("joining new room");
      await call.join({
        url: `https://hyperlink.daily.co/${roomData.id}`,
        //@ts-ignore
        userName: JSON.stringify({
          username: user?.display_name || user?.username,
          page: {
            title: roomData.name,
            ref: roomData.id,
          },
        }),
      });
      call.setLocalAudio(true);
      setInCall(roomData);
    },
    [room, user]
  );

  const leaveCall = async () => {
    if (!room) return;
    await room.leave();
    setInCall(null);
  };
  const setLocalAudio = (mute: boolean) => {
    if (!room) return;
    room.setLocalAudio(mute);
  };
  const setLocalVideo = (video: boolean) => {
    if (!room) return;
    room.setLocalVideo(video);
  };
  const setInputDevices = (devices: {
    audioDeviceId?: string;
    videoDeviceId?: string;
  }) => {
    if (!room) return;
    room.setInputDevices(devices);
  };

  const sendMessage = (msg: Message) => {
    if (!room) return;
    room.sendAppMessage(msg);
  };

  return (
    <CallContext.Provider
      value={{
        joinCall,
        leaveCall,
        participants,
        call,
        setLocalVideo,
        videoOn: !!room?.localVideo(),
        setLocalAudio,
        devices,
        setInputDevices,
        sendMessage,
        muted: !room?.localAudio(),
      }}
    >
      {props.children}
    </CallContext.Provider>
  );
};

export const JoinRoom = (props: { roomID: string; roomName: string }) => {
  let { joinCall, leaveCall, call, participants } = useContext(CallContext);
  let [loading, setLoading] = useState(false);

  let inCall = call && call.id === props.roomID;
  return (
    // JOIN CALL UI
    // If there is no one else in the room, return "You are the only one here right now... "
    <div
      style={{
        display: "grid",
        gridAutoFlow: "column",

        justifyItems: "left",
        gridGap: ".5rem",
      }}
    >
      {
        // <p className="italic">
        //   <span> You are with </span>
        //   {/* Names that are pulsing and bold are IN THE CALL */}
        //   <span className="animate-pulse font-bold"> Brendan, celine, </span>
        //   <span>Jinjin, and jared. </span>
      }

      {!inCall ? (
        <Secondary
          onClick={async (e) => {
            e.preventDefault();
            if (loading || inCall) return;
            setLoading(true);
            await joinCall({ id: props.roomID, name: props.roomName });
            setLoading(false);
          }}
        >
          Join Call
        </Secondary>
      ) : (
        <SecondaryDestructive
          onClick={(e) => {
            e.preventDefault();
            if (inCall) return leaveCall();
          }}
        >
          Leave Call
        </SecondaryDestructive>
      )}
      <div className="hidden">
        {participants.map((p) => (
          <Participant participant={p} key={p.user_id} />
        ))}
      </div>
    </div>
  );
};

const Participant = (props: { participant: DailyParticipant }) => {
  const audioEl = useRef<HTMLAudioElement>(null);
  useEffect(() => {
    if (!props.participant.audioTrack || !audioEl.current) return;
    audioEl.current.srcObject = new MediaStream([props.participant.audioTrack]);
  }, [props.participant.audioTrack, audioEl]);
  return props.participant.local ? null : (
    <audio autoPlay playsInline ref={audioEl} />
  );
};

const useDevices = () => {
  let [devices, setDevices] = useState<MediaDeviceInfo[]>([]);
  useEffect(() => {
    navigator.mediaDevices.enumerateDevices().then(setDevices);
    let listener = async () => {
      let devices = await navigator.mediaDevices.enumerateDevices();
      setDevices(devices);
    };
    navigator.mediaDevices.addEventListener("devicechange", listener);
    return () =>
      navigator.mediaDevices.removeEventListener("devicechange", listener);
  }, []);
  return devices;
};
