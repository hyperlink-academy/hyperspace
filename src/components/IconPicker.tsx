import { useEffect, useState } from "react";
import { colors } from "src/Tokens";

const Icons = [
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/Lightbulb.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/Sparkle.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/Picture.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/Video.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/MusicNote.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/Potion.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/Plant.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/Puzzle.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/Chart.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/MoneyBag.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/Camera.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/Numbers.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/Crayon.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/House.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/Book.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/Present.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/Earth.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/Shapes.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/Heart.png",
  "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/SpeechBubble.png",
];

export const IconPicker = (props: {
  icon: string;
  disabled?: boolean;
  setIcon: (icon: string) => void;
}) => {
  let [pickerOpen, setPickerOpen] = useState(false);
  let imgSrc = props.icon ? props.icon : Icons[0];
  useEffect(() => {
    if (!pickerOpen) return;
    let listener = () => {
      setPickerOpen(false);
    };
    window.addEventListener("click", listener);
    return () => window.removeEventListener("click", listener);
  }, [pickerOpen]);
  return (
    <div style={{ position: "relative" }}>
      <img
        alt=""
        height={32}
        width={32}
        src={imgSrc}
        className="cursor-pointer"
        onClick={() => {
          if (!props.disabled) setPickerOpen((o) => !o);
        }}
      />
      <div
        style={{
          display: pickerOpen ? undefined : "none",
          position: "absolute",
          top: "3rem",
          left: "-1.5rem",
        }}
      >
        <IconGrid
          onClick={(icon) => {
            props.setIcon(icon);
            setPickerOpen(false);
          }}
        />
      </div>
    </div>
  );
};

export const IconGrid = (props: { onClick: (icon: string) => void }) => {
  return (
    <div
      style={{
        display: "grid",
        gridGap: "16px",
        gridTemplateColumns: "repeat(auto-fill, 48px)",
        maxWidth: "350px",
        background: "white",
        padding: "1rem",
        border: `1px solid ${colors.borderColor}`,
        borderRadius: "2px",
      }}
    >
      {Icons.map((icon) => (
        <img
          src={icon}
          className="hover:bg-backgroundBlue p-2 rounded-sm cursor-pointer"
          alt={icon.split("/")[icon.length - 1]}
          key={icon}
          onClick={() => props.onClick(icon)}
        />
      ))}
    </div>
  );
};
