type Button = React.FC<
  Omit<
    React.DetailedHTMLProps<
      React.ButtonHTMLAttributes<HTMLButtonElement>,
      HTMLButtonElement
    >,
    "className"
  >
>;

export const Primary: Button = (props) => {
  return (
    <button
      className={`font-bold border-2 
    px-2 py-1 rounded-sm 
    text-textOnDark
    bg-textLink
    border-textLink 
    outline-none
    hover:bg-backgroundBlue
    hover:text-textLink
    focus:bg-backgroundBlue
    focus:outline-none
    focus:text-text-link
    active:text-textOnDark
    active:bg-textLink
    `}
      {...props}
    >
      {props.children}
    </button>
  );
};

export const Secondary: Button = (props) => {
  return (
    <button
      className={`font-bold border-2 
    px-2 py-1 rounded-sm 
    text-textLink border-textLink 
    outline-none
    hover:bg-backgroundBlue
    focus:bg-backgroundBlue
    focus:outline-none
    active:text-textOnDark
    active:bg-textLink
    `}
      {...props}
    >
      {props.children}
    </button>
  );
};

export const SecondaryDestructive: Button = (props) => {
  return (
    <button
      className={`font-bold border-2 rounded-sm 
    px-2 py-1     
    text-accentRed border-accentRed
    bg-backgroundWhite
    hover:bg-backgroundRed
    focus:outline-none focus:bg-backgroundRed
    active:text-textOnDarkactive:bg-accentRed
    `}
      {...props}
    >
      {props.children}
    </button>
  );
};

export const Tertiary: Button = (props) => {
  return (
    <button
      className={`
      px-2 py-1 
      text-textSecondary 
      border border-borderDefault rounded-sm
      hover:border-textLink hover:bg-backgroundBlue hover:text-textLink 
      focus:border-textLink focus:bg-backgroundBlue focus:text-textLink
      active:border-textLink active:bg-textLink active:text-textOnDark
    `}
      {...props}
    >
      {props.children}
    </button>
  );
};

export const Plain: Button = (props) => {
  return (
    <button
      className={`
      text-textLink
      font-bold
      hover:underline
      outline-none
      focus:outline-none
    `}
      {...props}
    >
      {props.children}
    </button>
  );
};
export const PlainDestructive: Button = (props) => {
  return (
    <button
      className={`
      text-accentRed
      font-bold
      hover:underline
      outline-none
      focus:outline-none
    `}
      {...props}
    >
      {props.children}
    </button>
  );
};
