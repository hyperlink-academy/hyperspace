import React, { forwardRef, useMemo } from "react";
import { parser } from "src/utils";
import { EntityLink } from "./EntityLink";
import Linkify from "linkifyjs/react";

export const RenderedTextBlock = forwardRef<
  HTMLPreElement,
  { text: string } & React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLPreElement>,
    HTMLPreElement
  >
>((props, ref) => {
  let renderedContent = useMemo(() => {
    let tokens = parser(props.text || " ");
    let components = tokens.map((t, index) => {
      if (t.type === "plain") return t.value;
      const key = [t.type, t.position, index].join("-");
      return (
        <span className="text-textDisabled" key={key}>
          [[
          <EntityLink entityName={t.value.slice(2, -2)}>
            {t.value.slice(2, -2)}
          </EntityLink>
          ]]
        </span>
      );
    });
    return components;
  }, [props.text]);

  return (
    <Linkify tagName="div">
      <pre ref={ref} {...props}>
        {renderedContent}
      </pre>
    </Linkify>
  );
});

RenderedTextBlock.displayName = "RenderedTextBlock";
