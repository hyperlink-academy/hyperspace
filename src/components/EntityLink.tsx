import Link from "next/link";
import { query as q } from "faunadb";
import { useFauna } from "src/hooks/useFauna";
import { useEffect, useRef, useState, createContext, useContext } from "react";
import { Facts } from "src/fauna/setup/Facts";
import { mutate } from "swr";
import { useRouter } from "next/router";
import { CreateNewEntity, Entity, entityQuery } from "src/fauna/queries/entity";

let NameToIdContext = createContext<{ [n: string]: string }>({});

export const LinkNameToIdProvider: React.FC<{
  namesToIds: { [n: string]: string };
}> = (props) => {
  return (
    <NameToIdContext.Provider value={props.namesToIds}>
      {props.children}
    </NameToIdContext.Provider>
  );
};

export const EntityLink: React.FC<{
  entityName: string;
  entityRef?: { id: string };
}> = (props) => {
  let namesToIds = useContext(NameToIdContext);
  // I receive a entity *name* I need to look it up and if it exists query it,
  // and else just create it
  //
  // QUESTION: What do I do when they click *before* a new thing is created? Do
  // I just wait?  Similarly, *when* do I create a new document? When the
  // transaction to update the value of the block is saved?  I need to create
  // link attributes, so that would be when I do I suppose. This means the
  // debounced save is maybe not the best approach as it would result in many cruft named entities.
  let fauna = useFauna();
  let prefetched = useRef(false);
  let [id, setID] = useState(props.entityRef?.id || "");
  let router = useRouter();
  let [loading, setLoading] = useState(false);
  useEffect(() => {
    if (id && loading) router.push("/" + id);
  }, [loading, id, router]);

  let prefetch = async () => {
    if (!fauna) return;
    if (prefetched.current) return;
    let id: string | undefined = props.entityRef
      ? props.entityRef?.id
      : namesToIds[props.entityName];

    let entity: Entity;
    if (id) {
      entity = await fauna.query(
        entityQuery(q.Ref(q.Collection("Entities"), id))
      );
    } else {
      entity = await fauna.query(
        q.Let(
          {
            match: q.Range(
              q.Match(q.Index(Facts.indexes.ave.name), ["content/title"]),
              props.entityName,
              props.entityName
            ),
          },
          q.If(
            q.Exists(q.Var("match")),
            entityQuery(q.Select(["data", "entity"], q.Get(q.Var("match")))),
            CreateNewEntity({ "content/title": props.entityName })
          )
        )
      );
    }
    setID(entity.ref.id);
    prefetched.current = true;

    mutate(entity.ref.id, entity);
    return entity;
  };

  if (!props.entityName) return null;
  return (
    <>
      <Link href={"/" + (props.entityRef ? props.entityRef.id : id)}>
        <a
          onClick={(e) => {
            if (!id && !loading) {
              e.preventDefault();
              setLoading(true);
            }
          }}
          onMouseEnter={prefetch}
          onTouchStart={prefetch}
        >
          {props.children}
        </a>
      </Link>
    </>
  );
};
