import { DailyParticipant } from "@daily-co/daily-js";
import { useContext, useEffect, useMemo, useState } from "react";
import { colors } from "src/Tokens";
import { MutedIcon, PeopleIcon, UnmutedIcon } from "./Icons";
import { CallContext } from "./JoinRoom";
import { EntityLink } from "./EntityLink";
import Link from "next/link";
import Router from "next/router";

export function CallHeader() {
  let {
    leaveCall,
    call,
    muted,
    setLocalAudio,
    videoOn,
    setLocalVideo,
    devices,
    participants,
    setInputDevices,
    sendMessage,
  } = useContext(CallContext);

  useEffect(() => {
    let listener = (e: KeyboardEvent) => {
      if (e.key === "m" && e.altKey) {
        e.preventDefault();
        setLocalAudio(muted);
      }
    };
    window.addEventListener("keydown", listener);
    return () => window.removeEventListener("keydown", listener);
  }, [muted, setLocalAudio]);

  if (!call) return null;
  return (
    <div
      onClick={() => {
        setLocalAudio(muted);
      }}
      className={`
        callUIWrapper
        sticky
        -mt-8
        mb-4
        top-0
        z-50`}
    >
      <style jsx>
        {`
          .callUIWrapper {
            font-size: 0.875rem;
          }

          p {
            font-size: 0.875rem;
          }

           {
            /* show mute help text on hover only! */
          }
          .callUIWrapper .callUIMuteHelpText {
            display: none;
          }
          .callUIWrapper:hover .callUIMuteHelpText {
            display: block;
          }

          /* interactive class cannot be applied to Participant Dropdown becaue it has been pulls out into another component. 
          If you make changes to this style, remember to change it for the Participant Dropdown as well. */
          .interactive {
            cursor: pointer;
            padding: 0.125rem 0.25rem;
            border-radius: 0.125rem;
          }

          .interactive:hover {
            background-color: ${muted === false
              ? `${colors.accentGreenHover}`
              : `${colors.grey15}`};
          }

          select::-ms-expand {
            display: none;
          }
          select {
            -webkit-appearance: none;
            appearance: none;
          }

          option {
            color: ${colors.textPrimary};
            font-family: monospace;
          }
        `}
      </style>

      <div
        style={{
          position: "relative",
          margin: "0 -9999rem",
          padding: ".5rem 9999rem",
          backgroundColor:
            muted === false ? `${colors.accentSuccess}` : `${colors.grey35}`,
        }}
      >
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "min-content auto auto",
            alignItems: "center",
          }}
        >
          <div
            style={{
              display: "grid",
              gridTemplateColumns: "repeat(7, max-content)",
              gridGap: ".25rem",
              alignItems: "center",
            }}
          >
            <p className="text-textOnDark">
              Calling from{" "}
              <Link href={`/${call.id}`}>
                <a
                  onClick={(e) => e.stopPropagation()}
                  className="interactive text-textOnDark font-bold"
                >
                  {call.name}
                </a>
              </Link>
            </p>

            <span className="text-textOnDark">|</span>

            <PeopleDropDown
              participants={participants.filter((p) => !p.local)}
              muted={muted}
            />

            <span className="text-textOnDark">|</span>

            <div
              style={{
                display: "grid",
                alignItems: "center",
                gridTemplateColumns: "auto auto",
                gridGap: "4px",
              }}
            >
              <button
                className="self-center focus:outline-none"
                onClick={(e) => {
                  e.stopPropagation();
                  setLocalVideo(!videoOn);
                }}
              >
                <img
                  src={videoOn ? "/img/video-on.png" : "/img/video-off.png"}
                  className="self-center"
                />
              </button>
              <MediaDeviceList
                devices={devices.filter((f) => f.kind === "videoinput")}
                onChange={(d) => setInputDevices({ videoDeviceId: d })}
              />
            </div>

            <span className="text-textOnDark">|</span>
            <div>
              <label className=" text-textOnDark" htmlFor="choose mic">
                Set Mic
              </label>
              <MediaDeviceList
                devices={devices.filter((f) => f.kind === "audioinput")}
                onChange={(d) => setInputDevices({ audioDeviceId: d })}
              />
            </div>
          </div>
          <button
            className="text-textOnDark interactive"
            onClick={(e) => {
              e.stopPropagation();
              sendMessage({
                type: "summon",
                page: Router.query.entity as string,
              });
            }}
          >
            summon
          </button>

          {/* LEAVE CALL BUTTON */}
          <button
            onClick={(e) => {
              leaveCall();
              e.stopPropagation();
            }}
            className="interactive font-bold text-textOnDark justify-self-end w-max"
          >
            Leave
          </button>
          {/* END LEAVE CALL BUTTON */}
        </div>

        {/* ON HOVER mute Help Text */}
        <div
          className={`
            callUIMuteHelpText
            absolute
            top-12
            left-1/2
            transform -translate-x-1/2
            px-2
            bg-backgroundApp
            `}
        >
          {muted === false ? (
            <small className="italic text-textSeconday">
              <span className="font-bold">Unmuted!</span> Click anywhere in this
              bar to mute (alt + m)
            </small>
          ) : (
            <small className="italic text-textSeconday">
              <span className="font-bold">Muted!</span> Click anywhere in this
              bar to unmute (alt + m)
            </small>
          )}
        </div>
        {/* END ON HOVER mute Help Text */}
      </div>
    </div>
  );
}

function PeopleDropDown(props: {
  participants: DailyParticipant[];
  muted: boolean;
}) {
  let [dropdownOpen, setPeopleDropDown] = useState(false);

  useEffect(() => {
    if (!dropdownOpen) return;
    let listener = () => setPeopleDropDown(false);
    window.addEventListener("click", listener);
    return () => {
      window.removeEventListener("click", listener);
    };
  }, [dropdownOpen]);

  return (
    <div
      className="relative"
      onClick={(e) => {
        setPeopleDropDown(!dropdownOpen);
        e.stopPropagation();
      }}
    >
      <style jsx>
        {`
          p {
            @apply text-sm;
          }
        `}
      </style>

      <div
        className={`
        interactive 
        text-textOnDark 
        grid 
        grid-flow-col 
        gap-2 
        items-center 
        px-2 py-1 
        rounded-sm 
        ${
          props.muted === false
            ? `hover:bg-accentGreenHover`
            : `hover:bg-grey15`
        }
        `}
      >
        {PeopleIcon}
        <p className="font-bold">{props.participants.length + 1}</p>
      </div>

      {dropdownOpen === true ? (
        <ul
          className={`
            absolute
            top-8
            -left-3
            bg-backgroundApp
            px-4
            py-2
            w-96
            text-textSecondary
            border
            rounded-sm
            border-borderDefault
            z-50
            `}
        >
          {props.participants.length === 0 ? (
            <p className="text-textSecondary italic">
              No one else is here right now
            </p>
          ) : (
            props.participants.map((p) => {
              return <CallParticipant key={p.user_id} {...p} />;
            })
          )}
        </ul>
      ) : null}
    </div>
  );
}

const MediaDeviceList = (props: {
  devices: MediaDeviceInfo[];
  onChange: (v: string) => void;
}) => {
  return (
    <select
      className="interactive bg-transparent bg-none text-textOnDark font-bold focus:outline-none"
      style={{ maxWidth: "64px" }}
      name="choose mic"
      onClick={(e) => e.stopPropagation()}
      onChange={(e) => {
        props.onChange(e.currentTarget.value);
      }}
    >
      {props.devices.map((d) => (
        <option
          value={d.deviceId}
          key={d.deviceId}
          className="text-textPrimary"
        >
          {d.label}
        </option>
      ))}
    </select>
  );
};

function CallParticipant(props: DailyParticipant) {
  let participantData = useMemo(() => {
    try {
      let data = JSON.parse(props.user_name);
      return data as { username: string; page: { title: string; ref: string } };
    } catch (e) {
      return undefined;
    }
  }, [props.user_name]);

  return (
    <li
      style={{
        display: "grid",
        gridTemplateColumns: "max-content auto",
        gridGap: "0.5rem",
        alignItems: "center",
        padding: "0.25rem 0",
      }}
    >
      {props.audio ? (
        <span className="text-accentGreen">{UnmutedIcon}</span>
      ) : (
        <span>{MutedIcon}</span>
      )}
      <small>
        {participantData?.username || "Someone"}
        {!participantData ? null : (
          <span>
            {" "}
            is in{" "}
            <EntityLink
              entityName={participantData?.page.title || ""}
              entityRef={{ id: participantData?.page.ref || "" }}
            >
              {participantData?.page.title}
            </EntityLink>
          </span>
        )}
      </small>
    </li>
  );
}
