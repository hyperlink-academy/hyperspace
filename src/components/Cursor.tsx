import { useContext, useEffect, useMemo, useRef, useState } from "react";
import { useAuthentication } from "src/auth";
import { useAudioStreamVolume } from "src/hooks/useAudioStreamVolume";
import { useEntity } from "src/hooks/useEntity";
import { useMediaQuery } from "src/hooks/utils";
import { colors } from "src/Tokens";
import { CallContext } from "./JoinRoom";

export function CursorRandomOuterColor(): string {
  let CursorOuterColorArray = [
    "#036B45",
    "#054097",
    "#871B91",
    "#CC451A",
    "#802E00",
    "#D5A600",
  ];

  return CursorOuterColorArray[
    Math.floor(Math.random() * CursorOuterColorArray.length)
  ];
}

export function CursorRandomInnerColor(): string {
  let CursorInnerColorArray = [
    "#D4EAE2",
    "#D1E3FF",
    "#EEDDFF",
    "#FFD6D6",
    "#FFE2C0",
    "#FEFCBE",
  ];

  return CursorInnerColorArray[
    Math.floor(Math.random() * CursorInnerColorArray.length)
  ];
}

type Cursor = {
  x: number;
  y: number;
  innerColor: string;
  outerColor: string;
  username: string;
};

export function Cursors() {
  const { presence, send, socket } = useEntity();
  let { data: user } = useAuthentication();
  let colors = useRef({
    innerColor: CursorRandomInnerColor(),
    outerColor: CursorRandomOuterColor(),
  });
  let mobile = useMediaQuery("(max-width:896px)");

  const [localCursor, setLocalCursor] = useState<Cursor>();
  useEffect(() => {
    let listener = (msg: any) => {
      if (!user) return;
      let messages = JSON.parse(msg.data);
      if (messages.find((m: any) => m.type === "presence-poke")) {
        setLocalCursor((l) => {
          if (!user) return;
          send({
            type: "presence-update",
            id: user.id,
            data: { cursor: l },
          });
          return l;
        });
      }
    };
    socket?.addEventListener("message", listener);
    return () => {
      socket?.removeEventListener("message", listener);
    };
  }, [socket]);
  let throttled = useRef(false);
  useEffect(() => {
    if (!user || !localCursor) return;
    if (!throttled.current) {
      send({
        type: "presence-update",
        id: user.id,
        data: { cursor: localCursor },
      });
      throttled.current = true;
      setTimeout(() => {
        throttled.current = false;
      }, 150);
    }
  }, [localCursor]);

  let mouseData = useRef({
    x: 0,
    y: 0,
    lastScrollTop: undefined as undefined | number,
    lastScrollLeft: undefined as undefined | number,
  });
  useEffect(() => {
    if (!user || mobile) return;
    let innerColor = colors.current.innerColor;
    let outerColor = colors.current.outerColor;
    let mouselistener = (e: MouseEvent) => {
      mouseData.current = {
        x: e.pageX,
        y: e.pageY,
        lastScrollLeft: undefined,
        lastScrollTop: undefined,
      };
      setLocalCursor({
        innerColor,
        outerColor,
        username: user?.display_name || user?.username || "Someone",
        x: e.pageX - window.innerWidth / 2,
        y: e.pageY,
      });
    };
    let scrollListener = (_e: Event) => {
      let xDiff = 0;
      let yDiff = 0;
      if (mouseData.current.lastScrollLeft) {
        xDiff = window.scrollX - mouseData.current.lastScrollLeft;
      }
      if (mouseData.current.lastScrollTop) {
        yDiff = window.scrollY - mouseData.current.lastScrollTop;
      }

      mouseData.current = {
        x: mouseData.current.x + xDiff,
        y: mouseData.current.y + yDiff,
        lastScrollLeft: window.scrollX,
        lastScrollTop: window.scrollY,
      };

      setLocalCursor({
        innerColor,
        outerColor,
        username: user?.display_name || user?.username || "Someone",
        x: mouseData.current.x + xDiff - window.innerWidth / 2,
        y: mouseData.current.y + yDiff,
      });
    };
    window.addEventListener("scroll", scrollListener);
    window.addEventListener("mousemove", mouselistener);

    return () => {
      window.removeEventListener("mousemove", mouselistener);
      window.removeEventListener("scroll", scrollListener);
    };
  }, [user, mobile]);
  let { participants } = useContext(CallContext);
  let usernames = useMemo(() => {
    return participants.map((p) => {
      try {
        return { ...p, username: JSON.parse(p.user_name).username };
      } catch (e) {
        return { ...p, username: "" };
      }
    });
  }, [participants]);

  let localVideo = participants.find((p) => p.local);
  if (mobile) return null;

  return (
    <div>
      {localVideo && localCursor ? (
        <Cursor
          local
          key={"local"}
          video={localVideo.videoTrack}
          outerColor=""
          innerColor=""
          left={localCursor.x + window.innerWidth / 2}
          top={localCursor.y}
          name=""
        />
      ) : null}
      {Object.keys(presence).map((id) => {
        let p = presence[id];
        if (!p || !p.cursor) return null;
        let cursor = p.cursor;
        let media = usernames.find((u) => u.username === cursor.username);
        return (
          <Cursor
            key={id}
            video={media?.videoTrack}
            audio={media?.audioTrack}
            outerColor={cursor.outerColor}
            innerColor={cursor.innerColor}
            top={cursor.y - 4}
            left={window.innerWidth / 2 + cursor.x - 6}
            name={cursor.username}
          />
        );
      })}
    </div>
  );
}

export const Cursor = (props: {
  local?: boolean;
  outerColor: string;
  video?: MediaStreamTrack;
  audio?: MediaStreamTrack;
  innerColor: string;
  name: string;
  top: number;
  left: number;
}) => {
  return (
    <div
      className="drop-shadow-md pointer-events-none"
      style={{
        imageRendering: "crisp-edges",
        top: 0,
        left: 0,
        transition: props.local
          ? ""
          : "transform 0.5s cubic-bezier(.17,.93,.38,1)",
        transform: `translateX(${props.left}px) translateY(${props.top}px)`,
        position: "absolute",
        zIndex: 9999,
      }}
    >
      {props.local
        ? null
        : CursorImage({
            outerColor: props.outerColor,
            innerColor: props.innerColor,
          })}
      {props.video && !props.video.muted ? (
        <Video
          video={props.video}
          audio={props.audio}
          color={props.outerColor}
          local={props.local}
        />
      ) : null}

      <div
        className={`text-sm px-1 font-bold py-0 rounded-sm relative left-4 -top-1.5`}
        style={{
          color: props.innerColor,
          width: "fit-content",
          backgroundColor: props.outerColor,
        }}
      >
        {props.name}
      </div>
    </div>
  );
};

const Video = (props: {
  video: MediaStreamTrack;
  audio?: MediaStreamTrack;
  color: string;
  local?: boolean;
}) => {
  const volume = useAudioStreamVolume(props.audio);
  const videoEl = useRef<HTMLVideoElement>(null);
  useEffect(() => {
    if (videoEl.current && props.video) {
      videoEl.current.srcObject = new MediaStream([props.video]);
    }
  }, [props.video]);

  return (
    <video
      autoPlay
      muted
      playsInline
      className="pointer-events-none"
      ref={videoEl}
      style={{
        position: "relative",
        transform: "rotateY(180deg)",
        WebkitTransform: "rotateY(180deg)",
        transition: "transform 0.5s cubic-bezier(.17,.93,.38,1)",
        top: props.local ? "25px" : "-8px",
        left: "-8px",
        borderRadius: "50%",
        border: "3px solid " + props.color,
        objectFit: "cover",
        width: `${volume > 10 ? 128 : 64}px`,
        height: `${volume > 10 ? 128 : 64}px`,
      }}
    />
  );
};
export function CursorImage(props: { outerColor: string; innerColor: string }) {
  return (
    <svg
      width="25"
      height="31"
      viewBox="0 0 25 31"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g filter="url(#filter0_dd)">
        <path
          className="outline"
          d="M5 2H4V22H5V23L9 23V22H10V21H11V20H12V19H13V18H20V17H21V13H20V12H19V11H18V10H17V9H16V8H15V7H14V6H13V5H12V4H11V3H10V2H9V1H5V2Z"
          fill={colors.appBackground}
        />
        <path
          className="outer"
          d="M8 3H6V21H8V20H9V19H10V18H11V17H12V16H19V14H18V13H17V12H16V11H15V10H14V9H13V8H12V7H11V6H10V5H9V4H8V3Z"
          fill={props.outerColor}
        />
        <path
          className="inner"
          d="M9 7H8V17H9V16H10V15H11V14H15V13H14V12H13V11H12V10H11V9H10V8H9V7Z"
          fill={props.innerColor}
        />
      </g>
      <defs>
        <filter
          id="filter0_dd"
          x="0"
          y="0"
          width="25"
          height="31"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          />
          <feMorphology
            radius="1"
            operator="erode"
            in="SourceAlpha"
            result="effect1_dropShadow"
          />
          <feOffset dy="2" />
          <feGaussianBlur stdDeviation="2" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.06 0"
          />
          <feBlend
            mode="normal"
            in2="BackgroundImageFix"
            result="effect1_dropShadow"
          />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          />
          <feOffset dy="4" />
          <feGaussianBlur stdDeviation="2" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"
          />
          <feBlend
            mode="normal"
            in2="effect1_dropShadow"
            result="effect2_dropShadow"
          />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect2_dropShadow"
            result="shape"
          />
        </filter>
      </defs>
    </svg>
  );
}
