export const PageLoader = () => {
  return (
    <>
      <style jsx>
        {`
          img {
            image-rendering: crips-edges;
            image-rendering: pixelated;
            margin: auto;
            padding-top: 32px;
            width: 300px;
          }
        `}
      </style>
      <img src="/img/loading.gif" />
    </>
  );
};
