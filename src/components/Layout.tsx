import { Spacing } from "../Tokens";
import styled from "@emotion/styled";

export const Box = styled("div")<{ padding?: Spacing[]; maxWidth?: number }>`
  ${(props) =>
    props.padding
      ? "padding: " + props.padding.map((p) => `${p}px`).join(" ")
      : ""};
  ${(props) => (props.maxWidth ? `max-width: ${props.maxWidth}px` : "")};
`;

export function Separator(props: { h?: boolean; className?: string }) {
  return (
    <hr
      className={`
      ${props.className}
        ${props.h === true ? "h-px " : "h-full w-px"}
        overflow-hidden 
        border-l 
        border-t-0
        border-b 
        border-dashed 
        border-borderDefault`}
    />
  );
}

export const HStack = styled(Box)<{
  gap: Spacing;
  columns?: "max-content" | "auto" | "min-content";
}>`
  display: grid;
  grid-auto-flow: column;
  grid-gap: ${(props) => props.gap}px;
  grid-auto-columns: ${(props) =>
    props.columns ? props.columns : "max-content"};
`;

export const VStack = styled(Box)<{
  gap: Spacing;
}>`
  display: grid;
  grid-gap: ${(props) => props.gap}px;
`;
