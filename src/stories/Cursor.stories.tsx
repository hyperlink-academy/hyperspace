import React from "react";
import { Story, Meta } from "@storybook/react";

import {
  Cursor,
  CursorRandomOuterColor,
  CursorRandomInnerColor,
} from "src/components/Cursor";

export default {
  title: "Example/Cursor",
  component: Cursor,
} as Meta;

const Template: Story<React.ComponentProps<typeof Cursor>> = (args) => (
  <Cursor {...args} />
);

export const Normal = Template.bind({});
Normal.args = {
  top: 10,
  left: 10,
  innerColor: CursorRandomInnerColor(),
  outerColor: CursorRandomOuterColor(),
  name: "Alexander",
};
