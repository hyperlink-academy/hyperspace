import React, { useState } from "react";
import { Story, Meta } from "@storybook/react";

import Textarea from "src/components/Textarea";

export default {
  title: "Example/Textarea",
  component: Textarea,
} as Meta;

const Template: Story<{ initialText: string }> = (args) => {
  let [value, setValue] = useState(args.initialText);
  return (
    <Textarea value={value} onChange={(e) => setValue(e.currentTarget.value)} />
  );
};

export const Normal = Template.bind({});
Normal.args = {
  initialText: "Hello world [[A preview]]",
};
