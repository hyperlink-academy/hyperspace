import React, { useState } from "react";
import { Story, Meta } from "@storybook/react";
import { IconPicker, IconGrid } from "src/components/IconPicker";

export default {
  title: "IconPicker",
  component: IconPicker,
} as Meta;

const Picker: Story<{}> = (_args) => {
  let [icon, setIcon] = useState("");
  return (
    <div style={{ display: "grid", justifyItems: "right" }}>
      <IconPicker icon={icon} setIcon={setIcon} />
    </div>
  );
};

export const IconPickerS = Picker.bind({});

const IconGridStory: Story = () => {
  let [selectedIcon, setSelectedIcon] = useState("No icon clicked");
  return (
    <>
      {selectedIcon}
      <IconGrid onClick={(icon) => setSelectedIcon(icon)} />
    </>
  );
};

export const IconGridS = IconGridStory.bind({});
