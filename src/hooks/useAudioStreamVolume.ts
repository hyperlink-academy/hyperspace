import { useEffect, useState } from "react";

export const useAudioStreamVolume = (mediaStream?: MediaStreamTrack) => {
  let [volume, setVolume] = useState(0);
  useEffect(() => {
    if (!mediaStream) return;
    var audioCtx = new AudioContext();
    let source = new MediaStreamAudioSourceNode(audioCtx, {
      mediaStream: new MediaStream([mediaStream]),
    });

    var analyser = audioCtx.createAnalyser();
    analyser.fftSize = 2048;

    var bufferLength = analyser.frequencyBinCount;

    // Connect the source to be analysed
    source.connect(analyser);

    let interval = setInterval(() => {
      var dataArray = new Uint8Array(bufferLength);
      analyser.getByteFrequencyData(dataArray);
      var values = 0;
      var average;

      var length = dataArray.length;

      // get all the frequency amplitudes
      for (var i = 0; i < length; i++) {
        values += dataArray[i];
      }

      average = values / length;
      setVolume(average);
    }, 200);
    return () => clearInterval(interval);
  }, [mediaStream]);
  return volume;
};
