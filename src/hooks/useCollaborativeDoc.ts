import { MutableRefObject, useEffect, useState } from "react";

export const useMutableTextarea = (
  textarea: MutableRefObject<HTMLTextAreaElement | null>,
  initialValue: string
) => {
  let [state, setState] = useState(initialValue);

  useEffect(() => {
    setState(initialValue);
  }, [initialValue]);

  const onChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setState(e.currentTarget.value);
  };
  return [
    state,
    onChange,
    (transaction: Transaction, offset: number = 0) => {
      if (!textarea.current) return;

      let cursorStart = textarea.current.selectionStart;
      let cursorEnd = textarea.current.selectionEnd;

      let [newValue, cursors] = modifyString(
        state,
        [cursorStart, cursorEnd],
        transaction
      );

      setState(newValue);
      textarea.current.value = newValue;
      textarea.current.setSelectionRange(
        cursors[0] + offset,
        cursors[1] + offset
      );
    },
  ] as const;
};

export type Transaction = (tx: {
  insert: (i: number, s: string) => void;
  delete: (i: number, l: number) => void;
}) => void;

export function modifyString(
  input: string,
  initialCursor: number[],
  transact: Transaction
): [string, number[]] {
  let output = input;
  let cursors = initialCursor;
  transact({
    insert: (i: number, s: string) => {
      output = output.slice(0, i) + s + output.slice(i);
      cursors = cursors.map((c) => {
        if (i < c) return c + s.length;
        return c;
      });
    },
    delete: (i: number, l: number) => {
      output = output.slice(0, i) + output.slice(i + l);
      cursors = cursors.map((c) => {
        if (i > c) return c - l;
        return c;
      });
    },
  });
  return [output, cursors];
}
