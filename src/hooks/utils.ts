import { useState } from "react";
import { useEffect } from "react";

export const useMediaQuery = (query: string) => {
  let [match, setMatch] = useState(false);
  useEffect(() => {
    let mediaQuery = window.matchMedia(query);
    setMatch(mediaQuery.matches);
    let listener = () => {
      setMatch(mediaQuery.matches);
    };
    mediaQuery.addEventListener("change", listener);
    return () => mediaQuery.removeEventListener("change", listener);
  }, [query]);
  return match;
};
