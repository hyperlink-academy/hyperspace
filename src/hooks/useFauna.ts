import { createContext, useContext } from "react";
import { Client } from "faunadb";
export let FaunaContext = createContext<Client | null>(null);

export const useFauna = () => {
  return useContext(FaunaContext);
};
