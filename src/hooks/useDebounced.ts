import { useRef, useEffect } from "react";

export const useDebouncedKey = () => {
  let scheduledCallbacks = useRef<{ [k: string]: () => void }>({});
  useEffect(() => {
    return () => {
      Object.values(scheduledCallbacks.current).forEach((c) => c());
    };
  }, []);
  return (key: string, callback: Function, delay: number) => {
    if (scheduledCallbacks.current[key]) scheduledCallbacks.current[key]();
    const handler = setTimeout(() => {
      callback();
    }, delay);
    scheduledCallbacks.current[key] = () => clearTimeout(handler);
  };
};

export const useDebouncedEffect = (
  callback: Function,
  delay: number,
  deps: any[]
) => {
  const firstUpdate = useRef(true);
  useEffect(() => {
    if (firstUpdate.current) {
      firstUpdate.current = false;
      return;
    }
    const handler = setTimeout(() => {
      callback();
    }, delay);

    return () => {
      clearTimeout(handler);
    };
  }, [delay, ...deps]);
};
