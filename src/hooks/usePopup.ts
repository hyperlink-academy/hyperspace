import { useState, useEffect, useRef } from "react";
export default function usePopup(initialIsOpen: boolean) {
  const [isOpen, setIsOpen] = useState(initialIsOpen);
  const ref = useRef<null | HTMLDivElement>(null);

  const handleClickOutside = (event: MouseEvent) => {
    if (!(event.target instanceof Element)) return;
    if (ref.current && !ref.current.contains(event.target)) {
      setIsOpen(false);
    }
  };

  useEffect(() => {
    document.addEventListener("click", handleClickOutside, true);
    return () => {
      document.removeEventListener("click", handleClickOutside, true);
    };
  });

  return { ref, isOpen, setIsOpen };
}
