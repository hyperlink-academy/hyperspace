import { query as q, values } from "faunadb";
import useSWR from "swr";
import {
  useEffect,
  useMemo,
  useRef,
  useState,
  createContext,
  useContext,
  useCallback,
} from "react";
import { useAuthentication } from "src/auth";
import {
  CreateEntityFunction,
  DeleteEntityFunction,
  Entities,
} from "src/fauna/setup/Entities";
import { Entity, entityQuery } from "src/fauna/queries/entity";
import { useFauna } from "src/hooks/useFauna";
import { blockID, uuidv4 } from "src/utils";
import { CreateFactFunction, ScanIndex } from "src/fauna/setup/Facts";
import { generateKeyBetween } from "src/fractional-indexing";
import { Block } from "src/fauna/queries/block";
import { useRouter } from "next/router";

type Cursor = {
  x: number;
  y: number;
  innerColor: string;
  outerColor: string;
  username: string;
};
function useEntityLogic(entityID: string | undefined) {
  let { data: user } = useAuthentication();
  let history = useHistory();
  let router = useRouter();
  let fauna = useFauna();
  let [presence, setPresence] = useState<{ [id: string]: { cursor?: Cursor } }>(
    {}
  );
  let { data: entity, mutate } = useSWR<Entity | undefined>(
    !user || !fauna || !entityID ? null : entityID,
    async (key: string) => {
      if (!fauna || !key) return;
      try {
        let doc: Entity = await fauna.query(
          entityQuery(
            key === "home"
              ? q.Select(["data", "homepage"], q.Get(q.CurrentIdentity()))
              : q.Ref(q.Collection(Entities.name), key)
          )
        );
        return doc;
      } catch (e) {
        console.log(e);
      }
    }
  );

  let [reconnectSocket, setReconnectSocket] = useState({});
  let [socket, setSocket] = useState<WebSocket | null>(null);
  useEffect(() => {
    if (!entity) return;
    let websocket = new WebSocket(
      `wss://${process.env.NEXT_PUBLIC_WORKER_URL}/entity/${entity.ref.id}`
    );
    setSocket(websocket);

    websocket.onclose = (e) => {
      if (e.wasClean) return;
      let called = false;
      let listener = () => {
        if (called) return;
        called = true;
        setReconnectSocket({});
        window.removeEventListener("mousemove", listener);
        window.removeEventListener("keypress", listener);
      };
      window.addEventListener("mousemove", listener);
      window.addEventListener("keypress", listener);
    };
    websocket.binaryType = "arraybuffer";
    websocket.addEventListener("message", (message) => {
      if (message.data instanceof ArrayBuffer) return;
      let messages = JSON.parse(message.data);

      if (messages.find((m: any) => m.type === "mutate")) mutate();
      if (messages.find((m: any) => m.type === "poke")) mutate();

      let presenceUpdates = messages.filter(
        (m: any) => m.type === "presence-update"
      );
      if (presenceUpdates.length > 0) {
        setPresence((p) => {
          let newPresence = { ...p };
          presenceUpdates.forEach((p: any) => {
            newPresence[p.id] = p.data;
          });
          return newPresence;
        });
      }
    });

    return () => {
      setPresence({});
      if (user)
        websocket.send(
          JSON.stringify({ type: "presence-update", id: user.id, data: null })
        );
      websocket.close();
    };
  }, [entity?.ref.id, reconnectSocket]);

  let [temporaryBlocks, setTemporaryBlocks] = useState<Block[]>([]);
  let temporaryBlocksRef = useRef<Block[]>([]);
  useEffect(() => {
    setTemporaryBlocks([]);
  }, [entity?.ref.id]);
  useEffect(() => {
    temporaryBlocksRef.current = temporaryBlocks;
  }, [temporaryBlocks]);

  let blocks = useMemo(() => {
    let blocks = (entity?.blocks || [])
      .map((b) => {
        let tempID = temporaryBlocks.find((t) =>
          b.blockEntity ? t.blockEntity?.id === b.blockEntity.id : false
        );
        if (!tempID) return b;
        return {
          ...b,
          tempID: tempID.tempID,
        };
      })
      .concat(
        temporaryBlocks.filter(
          (t) =>
            !(
              t.blockEntity &&
              entity?.blocks.find(
                (b) => t.blockEntity && b.blockEntity?.id === t.blockEntity.id
              )
            )
        )
      )
      .sort((a, b) => {
        if (a.position.value === b.position.value)
          return blockID(a) > blockID(b) ? 1 : -1;
        return a.position.value > b.position.value ? 1 : -1;
      });
    return blocks;
  }, [entity, temporaryBlocks]);

  const createBlock = useCallback(
    async (block: {
      afterTemp?: (tempID: string) => void;
      index: number;
      content?: string;
      image?: string;
    }) => {
      if (!entity || !fauna) return;
      let position = generateKeyBetween(
        blocks[block.index]?.position.value || null,
        blocks[block.index + 1]?.position.value || null
      );
      let tempID = uuidv4();
      setTemporaryBlocks((e) => [
        ...e,
        {
          temporary: true,
          tempID,
          links: [],
          contentImage: block.image
            ? {
                value: block.image,
              }
            : undefined,
          contentText: {
            value: block.content || "",
          },
          position: {
            value: position,
          },
        },
      ]);
      if (block.afterTemp) block.afterTemp(tempID);

      let newBlockEntity: values.Ref = await fauna.query(
        q.Let(
          {
            newEntity: q.Select("ref", CreateEntityFunction.call({})),
          },
          q.Do(
            block.image
              ? CreateFactFunction.call({
                  entity: q.Var("newEntity") as values.Ref,
                  attribute: "content/image",
                  value: block.image,
                })
              : CreateFactFunction.call({
                  entity: q.Var("newEntity") as values.Ref,
                  attribute: "content/text",
                  value: block.content || "",
                }),
            CreateFactFunction.call({
              entity: q.Var("newEntity") as values.Ref,
              attribute: "block/position",
              value: position,
            }),
            CreateFactFunction.call({
              entity: q.Var("newEntity") as values.Ref,
              attribute: "block/parent",
              value: entity.ref,
            }),
            q.Var("newEntity")
          )
        )
      );
      // If the block has been deleted, then don't mutate
      if (!temporaryBlocksRef.current.find((b) => b.tempID === tempID)) {
        await fauna.query(DeleteEntityFunction.call(newBlockEntity));
      } else {
        if (socket)
          socket.send(
            JSON.stringify({
              type: "mutate",
            })
          );
        setTemporaryBlocks((temp) =>
          temp.map((t) => {
            if (t.tempID === tempID) {
              return { ...t, blockEntity: newBlockEntity };
            }
            return t;
          })
        );
      }
      mutate();
    },
    [mutate, entity, fauna]
  );

  const deleteBlock = useCallback(
    async (id: string | values.Ref) => {
      mutate(async (e) => {
        if (!e) return;
        return {
          ...e,
          blocks: e.blocks.filter((b) => {
            if (id instanceof values.Ref) return id.id !== b.blockEntity?.id;
            return id !== b.tempID;
          }),
        };
      }, false);
      setTemporaryBlocks((t) =>
        t.filter((b) => {
          if (id instanceof values.Ref) return id.id !== b.blockEntity?.id;
          return id !== b.tempID;
        })
      );
      if (id instanceof values.Ref && fauna) {
        await fauna.query(DeleteEntityFunction.call(id));
        socket?.send(
          JSON.stringify({
            type: "mutate",
          })
        );
        mutate();
      }
    },
    [mutate, fauna]
  );

  const deleteEntity = useCallback(async () => {
    if (!fauna) return;
    if (!entity) return;
    await fauna.query(
      q.Do(
        q.Map(
          q.Select(
            "data",
            q.Paginate(ScanIndex("vae", entity.ref, "block/parent"))
          ),
          q.Lambda((vae) =>
            DeleteEntityFunction.call(q.Select(1, vae) as values.Ref)
          )
        ),
        DeleteEntityFunction.call(entity.ref)
      )
    );
    if (history[0]) router.push(history[0]);
    if (!history[0] || history[0] === router.asPath) router.push("/home");
    mutate();
  }, [fauna, entity]);

  let send = useCallback(
    (msg: object) => {
      if (socket?.readyState === 1) {
        socket.send(JSON.stringify(msg));
      }
    },
    [socket]
  );

  return {
    socket,
    entity,
    mutate,
    send,
    blocks,
    createBlock,
    deleteBlock,
    deleteEntity,
    presence,
  };
}
let EntityContext = createContext<ReturnType<typeof useEntityLogic>>({} as any);
export const useEntity = () => useContext(EntityContext);
export const EntityProvider: React.FC = (props) => {
  let router = useRouter();
  let entity = useEntityLogic(router.query.entity as string);
  return (
    <EntityContext.Provider value={entity}>
      {props.children}
    </EntityContext.Provider>
  );
};

const useHistory = () => {
  let [historyItems, setHistoryItems] = useState<string[]>([]);
  let router = useRouter();
  useEffect(() => {
    setHistoryItems((items) => items.concat([router.asPath]).slice(-2));
  }, [router.asPath]);
  return historyItems;
};
