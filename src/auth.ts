import { ServerResponse, IncomingMessage } from "http";
import useSWR from "swr";
import cookie from "cookie";

export type Token = {
  secret: string;
  username: string;
  temp?: boolean;
  display_name?: string;
  id: string;
};
const AUTHKEY = "localstorage:auth";

export const useAuthentication = () => {
  return useSWR<Token>(AUTHKEY, async () => {
    let res = await fetch("/api/whoami");
    let result = await res.json();
    return result.token;
  });
};

export function loginCookie(token: Token) {
  return cookie.serialize("hyperspace_login_token", JSON.stringify(token), {
    path: "/",
    expires: new Date(Date.now() + 1000 * 60 * 60 * 24 * 30), // 30 days
    httpOnly: true,
    sameSite: "lax",
  });
}

export function getToken(req: IncomingMessage) {
  const cookies = req.headers.cookie;
  if (!cookies) return;

  const { hyperspace_login_token } = cookie.parse(cookies);
  if (hyperspace_login_token)
    return JSON.parse(hyperspace_login_token) as Token;
  return;
}

export function removeToken(res: ServerResponse) {
  res.setHeader(
    "Set-Cookie",
    cookie.serialize("hyperspace_login_token", "", {
      path: "/",
      expires: new Date(Date.now() - 1000),
      httpOnly: false,
    })
  );
}
