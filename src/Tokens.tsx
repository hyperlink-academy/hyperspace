export const colors = {
  grey95: "#EDECEA",
  grey90: "#E5E5E5",
  grey80: "#CCCCCC",
  grey55: "#8C8C8C",
  grey35: "#595959",
  grey15: "#272727",
  appBackground: "#FDFCFA",
  textSecondary: "#595959",
  textPrimary: "#272727",
  borderColor: "#8C8C8C",
  borderSelected: "blue",
  backgroundRed: "#F9EBE8",
  accentRed: "#C23C1E",
  accentSuccess: "#348C1E",
  accentGreenHover: "#2A7318",
  accentPeach: "#F8EFE2",
  accentLightBlue: "#F0F7FA",
  linkHover: "#00008B",
};

export type Spacing = 0 | 4 | 8 | 16 | 32 | 64 | 256;
export type Widths = 400 | 640 | 1024;

export const Mobile = "@media(max-width:420px)";
export const Tablet = "@media(max-width:768px)";
