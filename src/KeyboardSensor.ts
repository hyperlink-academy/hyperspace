import { createContext, useContext, useEffect } from "react";
import { SensorAPI } from "react-beautiful-dnd";

export const moveBlockFunctionContext = createContext<{
  moveBlock?: (item: string, direction: "up" | "down") => void;
  setMoveBlock: (
    func: () => (item: string, direction: "up" | "down") => void
  ) => void;
}>({ setMoveBlock: (_func) => {} });

export const keyboardMovementSensor = (api: SensorAPI) => {
  let moveBlock = useContext(moveBlockFunctionContext);
  useEffect(() => {
    if (moveBlock.moveBlock) return;
    moveBlock.setMoveBlock(() => (item, direction) => {
      const preDrag = api.tryGetLock(item);
      if (!preDrag) return;
      const drag = preDrag.snapLift();
      if (direction === "up") drag.moveUp();
      else drag.moveDown();
      drag.drop();
    });
  }, [moveBlock.moveBlock]);
};
