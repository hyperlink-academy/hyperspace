import {
  CreateFactFunction,
  Facts,
  UpdateOrCreateFact,
  UpdateFactFunction,
} from "src/fauna/setup/Facts";

import { query as q, values } from "faunadb";
import React, { useEffect, useState } from "react";
import { DragDropContext, Droppable, DropResult } from "react-beautiful-dnd";

import { useAuthentication } from "src/auth";
import { useDebouncedEffect, useDebouncedKey } from "src/hooks/useDebounced";
import { useEntity } from "src/hooks/useEntity";
import { generateKeyBetween } from "src/fractional-indexing";
import { blockID } from "src/utils";
import { useFauna } from "src/hooks/useFauna";
import { BlockComponent } from "src/components/Block";
import { JoinRoom } from "src/components/JoinRoom";
import { EntityLink } from "src/components/EntityLink";
import {
  keyboardMovementSensor,
  moveBlockFunctionContext,
} from "src/KeyboardSensor";
import { Block } from "src/fauna/queries/block";
import { Separator } from "src/components/Layout";
import { colors } from "src/Tokens";
import { RenderedTextBlock } from "src/components/RenderedTextBlock";
import { IconPicker } from "src/components/IconPicker";
import { DeleteIcon, MoreOptionsLargeIcon } from "src/components/Icons";
import { CreateBlockButton } from "src/components/CreateBlock";
import Textarea from "src/components/Textarea";
import Head from "next/head";
import { Entity } from "src/fauna/queries/entity";
import { Modal } from "src/components/Modal";
import { Plain, SecondaryDestructive } from "src/components/Buttons";
import { PageLoader } from "src/components/PageLoader";
import usePopup from "src/hooks/usePopup";

export default function SpacesPage() {
  let { data: user } = useAuthentication();
  let fauna = useFauna();
  let { entity, mutate, send, blocks, createBlock, deleteEntity, deleteBlock } =
    useEntity();

  let [moveBlock, setMoveBlock] =
    useState<((item: string, direction: "up" | "down") => void) | undefined>();
  let callWithDebounce = useDebouncedKey();

  const onDragEnd = (result: DropResult) => {
    if (!entity || !fauna) return;
    let destinationIndex = result.destination?.index;
    let sourceIndex = result.source.index;
    if (destinationIndex === undefined) return;
    if (destinationIndex === sourceIndex) return;

    let block = blocks.find((b) => blockID(b) === result.draggableId);
    if (!block) return;

    let newList = Array.from(blocks);
    newList.splice(sourceIndex, 1);
    let newPosition = generateKeyBetween(
      newList[destinationIndex - 1]?.position.value || null,
      newList[destinationIndex]?.position.value || null
    );

    // If positions are stored in the map, I don't need to reset this at all
    mutate((e) => {
      if (!e) return;
      return {
        ...e,
        blocks: e.blocks.map((b) => {
          if (blockID(b) === result.draggableId) {
            return { ...b, position: { value: newPosition } };
          }
          return b;
        }),
      };
    }, false);
    if (block.position.factRef) {
      callWithDebounce(
        blockID(block),
        async () => {
          if (!fauna || !block?.position.factRef) return;
          fauna
            .query(UpdateFactFunction.call(block.position.factRef, newPosition))
            .then(() => {
              send({ type: "mutate" });
              mutate();
            });
        },
        600
      );
    }
  };

  if (entity === undefined) return <PageLoader />;
  if (entity === null) return "404";

  return (
    <moveBlockFunctionContext.Provider value={{ moveBlock, setMoveBlock }}>
      <Head>
        <title>{`h_ ${entity.title?.value}`}</title>
      </Head>
      <DragDropContext onDragEnd={onDragEnd} sensors={[keyboardMovementSensor]}>
        {/* DOC HEADER : TITLE, PAGE ICON, DELETE */}
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "auto auto min-content",
            gridGap: "1rem",
            alignItems: "start",
            position: "relative",
            marginTop: "2.5rem",
            marginBottom: ".5rem",
            zIndex: 0,
          }}
        >
          <div className="grid grid-cols-[2rem,auto] gap-5">
            <IconPicker
              disabled={!user || user.temp}
              icon={entity.icon?.value || ""}
              setIcon={async (icon) => {
                if (!fauna || !entity) return;
                mutate(
                  { ...entity, icon: { ...entity.icon, value: icon } },
                  false
                );
                await fauna.query(
                  UpdateOrCreateFact(entity.ref, "content/icon", icon)
                );
                mutate();
                send({ type: "mutate" });
              }}
            />
            <Title
              lockEditing={!user || !!user.temp}
              value={entity.title?.value ? entity.title.value : ""}
              mutate={() => {
                mutate();
                send({ type: "mutate" });
              }}
              entity={entity.ref}
            />
          </div>
          {!user || user.temp ? null : (
            <>
              <div className="justify-self-end">
                <JoinRoom
                  roomID={entity.ref.id}
                  roomName={entity.title?.value || ""}
                />
              </div>
              <div
                style={{
                  justifySelf: "end",
                  paddingTop: "4px",
                }}
              >
                <MoreOptions deleteEntity={deleteEntity} />
              </div>
            </>
          )}
        </div>

        <Droppable droppableId="main">
          {(provided) => {
            return (
              <div
                className="blockListWrapper"
                //RENDER ITEMS IN TEXT BLOCK LIST
                {...provided.droppableProps}
                ref={provided.innerRef}
              >
                {!entity ? null : (
                  <BlockList
                    lockEditing={!user || !!user.temp}
                    deleteBlock={deleteBlock}
                    entityID={entity?.ref.id}
                    blocks={blocks}
                    mutate={mutate}
                    send={send}
                    createBlock={createBlock}
                  />
                )}

                {provided.placeholder}
              </div>
            );
          }}
        </Droppable>

        <hr />

        {/* RENDER BACKLINKS */}
        <BackLinks
          backReferences={entity.backReferences}
          title={entity.title?.value || ""}
        />
      </DragDropContext>
    </moveBlockFunctionContext.Provider>
  );
}

function BackLinks(props: {
  backReferences: Entity["backReferences"];
  title: string;
}) {
  let blocklinkedBlocks = props.backReferences.filter((b) =>
    /^\[\[[^\[\n\]]*\]\]$/.test(b.block.contentText?.value || "")
  );
  let inlineLinkedBlocks = props?.backReferences.filter(
    (b) => !/^\[\[[^\[\n\]]*\]\]$/.test(b.block.contentText?.value || "")
  );

  return (
    <div
      className="
          grid
          grid-flow-row 
          gap-4
          "
    >
      {blocklinkedBlocks.length === 0 ? null : (
        <h3 className="pt-8">Linked From...</h3>
      )}
      {blocklinkedBlocks.map((backLink) => (
        <BacklinkedBlock
          currentEntityTitle={props.title}
          key={backLink.block.blockEntity?.id}
          title={backLink.parent.title || ""}
          entityRef={backLink.parent.ref}
          icon={backLink.parent.icon?.data.value as string}
          content={""}
        />
      ))}

      {inlineLinkedBlocks.length === 0 ? null : (
        <h3 className="pt-8">Mentioned in...</h3>
      )}

      {inlineLinkedBlocks.map((backLink) => (
        <BacklinkedBlock
          currentEntityTitle={props.title}
          key={backLink.block.blockEntity?.id}
          title={backLink.parent.title || ""}
          entityRef={backLink.parent.ref}
          icon={backLink.parent.icon?.data.value as string}
          content={backLink.block.contentText?.value || ""}
        />
      ))}
    </div>
  );
}

function BacklinkedBlock(props: {
  title: string;
  currentEntityTitle?: string;
  entityRef: values.Ref;
  content: string;
  icon?: string;
}) {
  let [truncated, setTruncated] = useState(props.content.length > 400);
  let linkedPoint = props.content.indexOf(`[[${props.currentEntityTitle}]]`);
  let start = linkedPoint - 200 < 0 ? 0 : linkedPoint - 200,
    end = linkedPoint + 200;

  if (props.content.length < 500) {
    start = 0;
    end = props.content.length;
  }

  if (!props.content)
    return (
      <div
        className={`
      grid grid-cols-[2rem,auto] gap-4 items-center
      border border-borderDefault rounded-sm
      px-4 py-2`}
      >
        <img
          alt=""
          src={
            props.icon
              ? props.icon
              : "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/Lightbulb.png"
          }
          height={32}
          width={32}
        />
        <EntityLink entityName={props.title} entityRef={props.entityRef}>
          <h2>{props.title}</h2>
        </EntityLink>
      </div>
    );

  return (
    <div
      className={`
      grid grid-cols-[2rem,auto] gap-4
      border border-borderDefault rounded-sm 
      px-4 pt-4 pb-2`}
    >
      <img
        alt=""
        src={
          props.icon
            ? props.icon
            : "https://hyperlink-data.nyc3.cdn.digitaloceanspaces.com/icons/EmojiSet/Lightbulb.png"
        }
        height={32}
        width={32}
      />
      <div
        className={`
        grid
        grid-rows-[max-content,min-content,auto]
        gap-2`}
      >
        <EntityLink entityName={props.title} entityRef={props.entityRef}>
          {props.title}
        </EntityLink>
        <Separator h />
        <RenderedTextBlock
          text={
            truncated
              ? `...${props.content.slice(start, end)}...`
              : props.content
          }
          style={{ whiteSpace: "pre-wrap" }}
          className="text-textSecondary text-sm leading-relaxed"
        />
        <div className="text-sm justify-self-end italic text-textLink">
          {truncated ? (
            <button onClick={() => setTruncated(false)}>expand</button>
          ) : props.content.length > 400 ? (
            <button onClick={() => setTruncated(true)}>collapse</button>
          ) : null}
        </div>
      </div>
    </div>
  );
}

function BlockList(props: {
  entityID: string;
  blocks: Block[];
  lockEditing: boolean;
  mutate: ReturnType<typeof useEntity>["mutate"];
  send: (x: object) => void;
  deleteBlock: (id: string | values.Ref) => void;
  createBlock: ReturnType<typeof useEntity>["createBlock"];
}) {
  let [focusedBlock, setFocusedBlock] = useState<string | null>(
    props.blocks[0] ? blockID(props.blocks[0]) : null
  );
  return (
    <>
      {props.blocks.map((block, index) => {
        return (
          <BlockComponent
            mutate={props.mutate}
            send={props.send}
            index={index}
            key={blockID(block)}
            block={block}
            createBlock={props.createBlock}
            deleteBlock={async () =>
              props.deleteBlock(
                block.blockEntity ? block.blockEntity : block.tempID || ""
              )
            }
            setFocusedBlock={setFocusedBlock}
            focusedBlock={focusedBlock}
            blocks={props.blocks}
          />
        );
      })}

      <div className="mt-6 mb-12 mx-auto w-full text-center">
        {props.lockEditing ? null : (
          <CreateBlockButton
            onClick={() =>
              props.createBlock({
                index: props.blocks.length - 1,
                afterTemp: (tempID: string) => {
                  setFocusedBlock(tempID);
                },
              })
            }
          />
        )}
      </div>
    </>
  );
}

function Title(props: {
  lockEditing: boolean;
  value: string;
  entity: values.Ref;
  mutate: () => void;
}) {
  let fauna = useFauna();
  let [title, setTitle] = useState(props?.value);
  useEffect(() => setTitle(props.value), [props.value]);

  useDebouncedEffect(
    async () => {
      if (!fauna || !title || props.lockEditing) return;
      if (typeof title !== "string") return;
      await fauna.query(
        q.Let(
          {
            titleSet: q.Range(
              q.Match(q.Index(Facts.indexes.eav.name), props.entity),
              "content/title",
              "content/title"
            ),
          },
          q.If(
            q.IsEmpty(q.Var("titleSet")),
            CreateFactFunction.call({
              entity: props.entity,
              attribute: "content/title",
              value: title,
            }),
            q.Let(
              { titleFact: q.Get(q.Var("titleSet")) },
              q.Do(
                q.Foreach(
                  q.Select(
                    "data",
                    q.Paginate(
                      q.Range(
                        q.Match(
                          q.Index(Facts.indexes.ave.name),
                          "content/link"
                        ),
                        q.Select(["data", "value"], q.Var("titleFact")),
                        q.Select(["data", "value"], q.Var("titleFact"))
                      )
                    )
                  ),
                  q.Lambda((linkFact) =>
                    q.Let(
                      {
                        block: q.Select([1], linkFact),
                        content: q.Get(
                          q.Range(
                            q.Match(
                              q.Index(Facts.indexes.eav.name),
                              q.Var("block")
                            ),
                            "content/text",
                            "content/text"
                          )
                        ),
                        contentRef: q.Select("ref", q.Var("content")),
                        contentValue: q.Select(
                          ["data", "value"],
                          q.Var("content")
                        ),
                      },
                      q.Do(
                        UpdateFactFunction.call(q.Select([2], linkFact), title),
                        UpdateFactFunction.call(
                          q.Var("contentRef"),
                          q.ReplaceStr(
                            q.Var("contentValue"),
                            q.Concat([
                              "[[",
                              q.Select(["data", "value"], q.Var("titleFact")),
                              "]]",
                            ]),
                            q.Concat(["[[", title, "]]"])
                          )
                        )
                      )
                    )
                  )
                ),
                UpdateFactFunction.call(
                  q.Select("ref", q.Var("titleFact")),
                  title
                )
              )
            )
          )
        )
      );
      props.mutate();
    },
    1500,
    [title]
  );
  return (
    <Textarea
      /*TITLE BLOCK*/
      className="text-3xl font-bold bg-backgroundApp w-full "
      placeholder="Untitled"
      disabled={props.lockEditing}
      value={typeof title === "string" ? title : ""}
      onChange={(e) => setTitle(e.currentTarget.value)}
    />
  );
}

{
  /* MORE OPTIONS LIST*/
}

function MoreOptions(props: { deleteEntity: () => void }) {
  let { ref, isOpen, setIsOpen } = usePopup(false);

  return (
    <div className="h-full" ref={ref}>
      <style jsx>
        {`
          li,
          h4 {
            padding: 0.25rem 0.5rem;
          }

          li:hover {
            background-color: ${colors.accentLightBlue};
          }
        `}
      </style>

      <button
        className="relative h-full"
        onClick={() => {
          setIsOpen(!isOpen);
        }}
      >
        {MoreOptionsLargeIcon}
      </button>

      {isOpen === false ? null : (
        <ul
          className={`
            absolute 
            text-right
            border border-borderDefault rounded-sm
            bg-backgroundWhite
            py-2
            -right-2 top-12
          `}
        >
          <h3 className="px-2">Mods</h3>
          <li>Make Event</li>
          <Separator h className="mx-2 my-2" />
          <li>
            <DeletePage
              deleteEntity={() => props.deleteEntity()}
              closePopUp={() => setIsOpen(false)}
            />
          </li>
        </ul>
      )}
    </div>
  );
}

function DeletePage(props: {
  deleteEntity: () => void;
  closePopUp: () => void;
}) {
  let [isOpen, setDeletePageModal] = useState(false);
  return (
    <>
      <SecondaryDestructive
        onClick={() => {
          setDeletePageModal(true);
        }}
      >
        <div className="grid grid-cols-[max-content,max-content] gap-2 items-center">
          Delete Page
          {DeleteIcon}
        </div>
      </SecondaryDestructive>

      <Modal open={isOpen} onClose={() => setDeletePageModal(!isOpen)}>
        <div className="deletePageModalContent grid grid-auto-rows gap-4">
          <div className="grid grid-auto-rows gap-2">
            <h3>Delete this Page?</h3>
            <p> You cannot undo this action later!</p>
          </div>

          <Separator h />
          <div className="deletePageModalOptions grid grid-cols-[auto,max-content] justify-items-end gap-4">
            <Plain onClick={() => setDeletePageModal(!isOpen)}>Nevermind</Plain>
            <SecondaryDestructive
              onClick={() => {
                props.deleteEntity();
                setDeletePageModal(false);
                props.closePopUp();
              }}
            >
              Delete Page
            </SecondaryDestructive>
          </div>
        </div>
      </Modal>
    </>
  );
}
