import { NextApiRequest, NextApiResponse } from "next";
import crypto from "crypto";
import { query as q, Client } from "faunadb";
import { CreatePersonFunction, People } from "src/fauna/setup/People";
import { loginCookie } from "src/auth";

var client = new Client({ secret: process.env.FAUNA_SECRET || "" });

type SSOResponse = {
  requestID: string;
  username: string;
  email: string;
  display_name?: string;
  user: string;
};

export default async function SSOResponseHandler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  let { payload, signature } = req.query;

  const hmac1 = crypto.createHmac("sha256", process.env.SSO_SECRET || "");
  hmac1.update(Buffer.from(payload as string));
  let calculatedSignature = hmac1.digest("base64");
  if (calculatedSignature !== signature) return res.status(400).end();

  let sso_response: SSOResponse = JSON.parse(
    Buffer.from(payload as string, "base64").toString()
  );
  console.log(sso_response);

  let result: {
    token: { secret: string };
    redirect: string | false;
  } | null = await client.query(
    q.If(
      q.Exists(q.Ref(q.Collection("SSOTokens"), sso_response.requestID)),
      q.Let(
        {
          user: q.Match(
            q.Index(People.indexes.people_by_id.name),
            sso_response.user
          ),
        },
        {
          redirect: q.Select(
            ["data", "redirect"],
            q.Get(q.Ref(q.Collection("SSOTokens"), sso_response.requestID)),
            false
          ),
          token: q.If(
            q.IsEmpty(q.Var("user")),
            q.Let(
              {
                user: CreatePersonFunction.call({ id: sso_response.user }),
              },
              q.Create(q.Tokens(), { instance: q.Select("ref", q.Var("user")) })
            ),
            q.Create(q.Tokens(), {
              instance: q.Select(["ref"], q.Get(q.Var("user"))),
            })
          ),
        }
      ),
      null
    )
  );

  if (!result) return res.status(400).end();
  res.setHeader(
    "Set-Cookie",
    loginCookie({
      secret: result.token.secret,
      id: sso_response.user,
      username: sso_response.username,
      display_name: sso_response.display_name,
    })
  );
  res.redirect(result.redirect ? result.redirect : "/");
  res.end();
}
