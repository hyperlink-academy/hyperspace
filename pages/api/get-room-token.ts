import { NextApiRequest, NextApiResponse } from "next";

let headers = {
  "Content-Type": "application/json",
  AUTHORIZATION: `Bearer ${process.env.DAILY_API_KEY}`,
};

export default async function GetRoomTokenHandler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  let roomName = req.body.room;
  let roomDataRequest = await fetch(
    `https://api.daily.co/v1/rooms/${roomName}`,
    {
      method: "GET",
      headers,
    }
  );

  let room;
  if (roomDataRequest.status === 404) {
    console.log("creating room: " + roomName);
    let createRoomRequest = await fetch(`https://api.daily.co/v1/rooms`, {
      method: "POST",
      headers,
      body: JSON.stringify({
        name: roomName,
      }),
    });
    room = await createRoomRequest.json();
  } else {
    room = await roomDataRequest.json();
  }
  return res.json({ room });
}
