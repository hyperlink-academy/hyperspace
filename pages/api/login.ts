import querystring from "querystring";
import { NextApiRequest, NextApiResponse } from "next";
import crypto from "crypto";
import { query as q, Client, values } from "faunadb";
import { getToken } from "src/auth";

var client = new Client({ secret: process.env.FAUNA_SECRET || "" });

type SSORequest = {
  requestID: string;
  requestOrigin?: string;
};

export default async function LoginHandler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  let redirect = req.query.redirect as string;
  // need to redirect if they're already logged in
  let token = getToken(req);
  if (token && !token.temp)
    return res.redirect(redirect ? redirect : "/").end();

  const hmac1 = crypto.createHmac("sha256", process.env.SSO_SECRET || "");
  let loginToken: { ref: values.Ref } = await client.query(
    q.Create(q.Collection("SSOTokens"), {
      ttl: q.TimeAdd(q.Now(), 10, "minutes"),
      data: { redirect },
    })
  );

  let SSORequest: SSORequest = {
    requestID: loginToken.ref.id,
    requestOrigin: process.env.SSO_ORIGIN,
  };
  let payload = Buffer.from(JSON.stringify(SSORequest)).toString("base64");
  hmac1.update(Buffer.from(payload));
  let signature = hmac1.digest("base64");

  res.writeHead(307, {
    Location:
      `${process.env.MAIN_HYPERLINK_APP_URL}/api/hyperspace/sso?` +
      querystring.stringify({ payload, signature }),
  });
  res.end();
}
