import { Client, query } from "faunadb";
import { NextApiRequest, NextApiResponse } from "next";
import { getToken, removeToken } from "src/auth";

export default async function LogoutHandler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  let token = getToken(req);
  if (!token) return res.end();
  let fauna = new Client({ secret: token.secret });
  fauna.query(query.Logout(false));
  removeToken(res);
  res.end();
}
