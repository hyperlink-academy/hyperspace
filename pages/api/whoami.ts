import { NextApiHandler } from "next";
import { getToken, loginCookie, Token } from "src/auth";
import { TemporaryLogin } from "src/fauna/setup/People";
import { query as q, Client, values } from "faunadb";

var client = new Client({ secret: process.env.FAUNA_SECRET || "" });

type Response = {
  token: Token | null;
};
const whoami: NextApiHandler<Response> = async (req, res) => {
  let token = getToken(req);
  if (!token) {
    let result: { ref: values.Ref; secret: string } = await client.query(
      q.Create(q.Tokens(), {
        ttl: q.TimeAdd(q.Now(), 2, "days"),
        instance: q.Select(
          "ref",
          q.Create(q.Collection(TemporaryLogin.name), {
            data: {},
            ttl: q.TimeAdd(q.Now(), 2, "days"),
          })
        ),
      })
    );
    console.log(result);
    let token: Token = {
      temp: true,
      secret: result.secret,
      id: result.ref.id,
      username: result.ref.id,
      display_name: "Somebody",
    };
    res.setHeader("Set-Cookie", loginCookie(token));
    return res.json({ token });
  }
  return res.json({ token: token || null });
};
export default whoami;
