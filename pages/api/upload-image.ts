import { S3 } from "aws-sdk";
import { NextApiRequest, NextApiResponse } from "next";

const s3 = new S3({
  endpoint: "nyc3.digitaloceanspaces.com",
  accessKeyId: process.env.SPACES_KEY || "",
  secretAccessKey: process.env.SPACES_SECRET,
});

export default async function UploadImageHandler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  let imageID = req.body;
  if (await checkIfImageExists(imageID))
    return res.json({ exists: true, imageID });
  const url = s3.getSignedUrl("putObject", {
    Bucket: "hyperspace-uploads",
    Key: imageID,
    ACL: "public-read",
    ContentType: "image",
    Expires: 60 * 5,
  });
  return res.json({ url, imageID });
}

const checkIfImageExists = (file: string) => {
  return new Promise((resolve) => {
    s3.getObject(
      {
        Bucket: "hyperspace-uploads",
        Key: file,
      },
      (err) => {
        if (err) resolve(false);
        else resolve(true);
      }
    );
  });
};
