import "tailwindcss/tailwind.css";
import "src/styles.css";
import { AppHeader } from "src/components/AppHeader";
import React, { useEffect, useState } from "react";
import { Client } from "faunadb";
import { useAuthentication } from "src/auth";
import { CallProvider } from "src/components/JoinRoom";
import { FaunaContext } from "src/hooks/useFauna";
import { CallHeader } from "src/components/CallHeader";
import { EntityProvider } from "src/hooks/useEntity";
import { Cursors } from "src/components/Cursor";

// import { useMemo } from 'react';
// import { Client, values } from 'faunadb';
// import { CreateEntityFunction } from 'src/fauna/setup/Entities';
// import {useRouter} from 'next/router'
// import {useAuthentication} from 'src/auth'

// Create a liveblocks client

export default function App({ Component, pageProps }: any) {
  let [fauna, setFauna] = useState<null | Client>(null);
  let { data: user } = useAuthentication();
  useEffect(() => {
    setFauna(user ? new Client({ secret: user.secret }) : null);
  }, [user]);

  return (
    <div className="grid grid-auto-columns max-w-screen-md">
      <div className="grid grid-auto-rows">
        <FaunaContext.Provider value={fauna}>
          <EntityProvider>
            <CallProvider>
              <CallHeader />
              <AppHeader />
              {!!user ? <Cursors /> : null}
              <Component {...pageProps} />
            </CallProvider>
          </EntityProvider>
        </FaunaContext.Provider>
      </div>
      <div className="w-80"></div>
    </div>
  );
}

// const useHistory = () => {
//   let [historyItems, setHistoryItems] = useState<string[]>([]);
//   let router = useRouter();
//   useEffect(() => {
//     setHistoryItems((items) => items.concat([router.asPath]).slice(-2));
//   }, [router.asPath]);
//   return historyItems;
// };
