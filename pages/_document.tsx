import { resetIdCounter } from "downshift";
import Document, { Html, Head, Main, NextScript } from "next/document";
import { resetServerContext } from "react-beautiful-dnd";

class MyDocument extends Document {
  static async getInitialProps(ctx: any) {
    const initialProps = await Document.getInitialProps(ctx);
    resetServerContext();
    resetIdCounter();
    return { ...initialProps };
  }

  render() {
    return (
      <Html>
        <Head />
        <body style={{ overflowY: "scroll" }}>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
