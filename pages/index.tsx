import { useAuthentication } from "src/auth";

export default function App() {
  let { data } = useAuthentication();
  return <div>{data ? "Logged in" : "Logged out!"}</div>;
}
