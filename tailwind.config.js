module.exports = {
  mode: "jit",
  purge: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      transparent: "transparent",
      current: "currentColor",

      //TEXT COLORS
      textPrimary: {
        DEFAULT: "#272727",
      },
      textSecondary: {
        DEFAULT: "#595959",
      },
      textOnDark: {
        DEFAULT: "#FDFCFA",
      },
      textDisabled: {
        DEFAULT: "#8C8C8C",
      },
      textLink: {
        DEFAULT: "#0000FF",
      },
      textLinkHover: {
        DEFAULT: "#E1066F",
      },

      //ACCENT COLORS
      //for error text mostly
      accentRed: {
        DEFAULT: "#C23C1E",
      },
      //for success text mostly
      accentGreen: {
        DEFAULT: "#348C1E",
      },

      accentGreenHover: {
        DEFAULT: "#2A7318",
      },

      //BACKGROUND COLORS
      //jus the regular ol' background color
      backgroundApp: {
        DEFAULT: "#FDFCFA",
      },
      //background on error messages
      backgroundRed: {
        DEFAULT: "#F9EBE8",
      },
      //background on purely informational elements (like banners)
      backgroundPeach: {
        DEFAULT: "#F8EFE2",
      },
      //background on interactive elements (like cards or menu options)
      backgroundBlue: {
        DEFAULT: "#F0F7FA",
      },
      //background on inputs like search
      backgroundWhite: {
        DEFAULT: "#FFFFFF",
      },

      //BORDER COLORS
      borderDefault: {
        DEFAULT: "#8C8C8C",
      },
      borderFocus: {
        DEFAULT: "#272727",
      },

      //JUST A BUNCH OF GRAYS. # refers to lightness value. 95 = lightest, 15 = darkest
      grey95: {
        DEFAULT: "#EDECEA",
      },
      grey90: {
        DEFAULT: "#E5E5E5",
      },
      grey80: {
        DEFAULT: "#CCCCCC",
      },
      grey55: {
        DEFAULT: "#8C8C8C",
      },
      grey35: {
        DEFAULT: "#595959",
      },
      grey15: {
        DEFAULT: "#272727",
      },

      //DO NOT USE IN PRODUCTION. Test colors to aid development, ie, setting bg color on element to see edges of div. DO. NOT. USE. IN. PRODUCTION
      pink: {
        DEFAULT: "#E18181",
      },
      blue: {
        DEFAULT: "#48D1EF",
      },
    },
  },

  fontSize: {
    sm: ".875rem",
    base: "1.1rem",
    lg: "1.333rem",
    xl: "1.5rem",
    "2xl": "2rem",
    "3xl": "2.75rem",
  },
  boxShadow: {
    cursor:
      "0 4px 6px -1px rgba(0, 0, 0, 0.4), 0 2px 4px -1px rgba(0, 0, 0, 0.2)",
  },
  variants: {
    padding: ["responsive"],
    inset: ["responsive"],
  },
  plugins: [],
};
